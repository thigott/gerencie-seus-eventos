import 'package:form_field_validator/form_field_validator.dart';

class NotRequiredPatternValidator extends TextFieldValidator {
  final Pattern pattern;

  NotRequiredPatternValidator(this.pattern, {String errorText = "O campo deve ter no mínimo 6 caracteres!"})
      : super(errorText);

  @override
  bool isValid(String value) {
    if (value.isEmpty) return true;
    return hasMatch(pattern, value);
  }
}
