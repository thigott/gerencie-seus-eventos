import 'package:form_field_validator/form_field_validator.dart';
import 'package:gerencie_seu_evento/core/validators/cnpj.validator.dart';
import 'package:gerencie_seu_evento/core/validators/cpf.validator.dart';

class DocumentValidator extends TextFieldValidator {
  DocumentValidator({String errorText = "Documento inválido"})
      : super(errorText);

  @override
  bool isValid(String value) {
    if (value.length > 14) {
      return CNPJValidator().isValid(value);
    } else {
      return CPFValidator().isValid(value);
    }
  }
}
