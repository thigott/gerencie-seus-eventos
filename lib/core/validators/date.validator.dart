import 'package:form_field_validator/form_field_validator.dart';
import 'package:intl/intl.dart';

class CustomDateValidator extends TextFieldValidator {
  CustomDateValidator({
    String errorText = "Data inválida",
  }) : super(errorText);

  @override
  bool isValid(String value) {
    var flag = true;

    if (value.isEmpty) return true;

    if (!RegExp("([0-9]){2}\/([0-9]){2}\/([0-9]){4}").hasMatch(value)) return false;

    try {
      var date = DateFormat('dd/MM/yyyy').parse(value);
      if (date.isBefore(DateTime.now().subtract(Duration(days: 1)))) flag = false;
    } catch (e) {
      flag = false;
    }

    return flag;
  }
}
