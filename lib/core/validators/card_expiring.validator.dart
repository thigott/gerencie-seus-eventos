import 'package:form_field_validator/form_field_validator.dart';

class CardExpiringValidator extends TextFieldValidator {
  String errorText;

  CardExpiringValidator({
    this.errorText = "Validade inválida",
  }) : super(errorText);

  @override
  bool isValid(String value) {
    if (value.isEmpty) return false;
    if (!RegExp("([0-9]){2} / ([0-9]){4}").hasMatch(value)) return false;

    var date = value.split(" / ");
    var now = DateTime.now();

    int month = int.parse(date[0]);
    int year = int.parse(date[1]);
    int nowMonth = now.month;
    int nowYear = now.year;

    if (month < 1 || month > 12) return false;
    if (year < nowYear) return false;
    if (year == nowYear && month < nowMonth) return false;

    return true;
  }
}
