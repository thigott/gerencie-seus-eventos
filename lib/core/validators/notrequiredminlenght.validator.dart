import 'package:form_field_validator/form_field_validator.dart';

class NotRequiredMinLenghtValidator extends TextFieldValidator {
  final int min;

  NotRequiredMinLenghtValidator(this.min, {String errorText = "O campo deve ter no mínimo 6 caracteres!"})
      : super(errorText);

  @override
  bool isValid(String value) {
    if (value.isEmpty) return true;
    return value.length >= min;
  }
}
