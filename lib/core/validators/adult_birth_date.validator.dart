import 'package:form_field_validator/form_field_validator.dart';
import 'package:intl/intl.dart';

class AdultBirthDateValidator extends TextFieldValidator {
  AdultBirthDateValidator({
    String errorText = "Idade mínima de 12 anos.",
  }) : super(errorText);

  @override
  bool isValid(String value) {
    var flag = true;

    if (value.isEmpty) return true;

    if (!RegExp("([0-9]){2}\/([0-9]){2}\/([0-9]){4}").hasMatch(value))
      return false;

    try {
      var date = DateFormat('dd/MM/yyyy').parse(value);
      var now = DateTime.now();
      if (date.isAfter(DateTime(now.year - 12, now.month, now.day)))
        flag = false;
    } catch (e) {
      flag = false;
    }

    return flag;
  }
}
