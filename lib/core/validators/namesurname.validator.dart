import 'package:form_field_validator/form_field_validator.dart';

class NameSurnameValidator extends TextFieldValidator {
  String errorText;

  NameSurnameValidator({
    this.errorText = "Digite seu nome completo",
  }) : super(errorText);

  @override
  bool isValid(String value) {
    bool _flag = true;
    final name = value.split(" ");
    if (name.length < 2) return false;
    _flag = name[0].length > 1;
    _flag = name[1].length > 0;
    return _flag;
  }
}
