import 'package:form_field_validator/form_field_validator.dart';
import 'package:intl/intl.dart';

class BabyBirthDateValidator extends TextFieldValidator {
  BabyBirthDateValidator({
    String errorText = "Idade máxima 1 ano e 11 meses.",
  }) : super(errorText);

  @override
  bool isValid(String value) {
    var flag = true;

    if (value.isEmpty) return true;

    if (!RegExp("([0-9]){2}\/([0-9]){2}\/([0-9]){4}").hasMatch(value)) return false;

    try {
      var date = DateFormat('dd/MM/yyyy').parse(value);
      var now = DateTime.now();
      if (date.isBefore(DateTime(now.year - 1, now.month - 11, now.day))) flag = false;
    } catch (e) {
      flag = false;
    }

    return flag;
  }
}