import 'package:form_field_validator/form_field_validator.dart';
import 'package:intl/intl.dart';

class BirthDateValidator extends TextFieldValidator {
  BirthDateValidator({
    String errorText = "Data inválida",
  }) : super(errorText);

  @override
  bool isValid(String value) {
    var flag = true;

    if (value.isEmpty) return true;

    if (!RegExp("([0-9]){2}\/([0-9]){2}\/([0-9]){4}").hasMatch(value)) return false;

    try {
      var date = DateFormat('dd/MM/yyyy').parse(value);
      if (date.isAfter(DateTime.now())) flag = false;
    } catch (e) {
      flag = false;
    }

    return flag;
  }
}
