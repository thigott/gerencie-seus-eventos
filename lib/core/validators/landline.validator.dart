import 'package:form_field_validator/form_field_validator.dart';

class LandLineValidator extends TextFieldValidator {
  LandLineValidator({String errorText = "Telefone inválido"}) : super(errorText);

  @override
  bool isValid(String number) {
    if (number.isEmpty) return true;
    String cleanNumber = number.replaceAll(RegExp("[() -]"), "");
    return RegExp("(?:[14689][1-9]|2[12478]|3[1234578]|5[1345]|7[134579])[2-5][0-9]{7}").hasMatch(cleanNumber);
  }
}
