import 'package:form_field_validator/form_field_validator.dart';

class StateValidator extends TextFieldValidator {
  StateValidator({
    String errorText = "Estado inválido",
  }) : super(errorText);

  List<String> estados = [
    "AC",
    "AL",
    "AM",
    "AP",
    "BA",
    "CE",
    "DF",
    "ES",
    "GO",
    "MA",
    "MT",
    "MS",
    "MG",
    "PA",
    "PB",
    "PR",
    "PE",
    "PI",
    "RJ",
    "RN",
    "RO",
    "RS",
    "RR",
    "SC",
    "SE",
    "SP",
    "TO",
  ];

  @override
  bool isValid(String state) {
    return estados.contains(state);
  }
}
