import 'package:flutter/material.dart';
import 'package:form_field_validator/form_field_validator.dart';

class StringMathValidator extends TextFieldValidator {
  String errorText;
  String comparator;

  StringMathValidator({
    @required this.comparator,
    @required this.errorText,
  }) : super(errorText);

  @override
  bool isValid(String value) {
    return value == comparator;
  }
}
