import 'package:flutter/material.dart';
import 'package:form_field_validator/form_field_validator.dart';

class FieldMathValidator extends TextFieldValidator {
  String errorText;
  TextEditingController comparator;

  FieldMathValidator({
    @required this.comparator,
    @required this.errorText,
  }) : super(errorText);

  @override
  bool isValid(String value) {
    return value == comparator.text;
  }
}
