import 'package:form_field_validator/form_field_validator.dart';
import 'package:get/utils.dart';

class CPFValidator extends TextFieldValidator {
  CPFValidator({String errorText = "CPF inválido"}) : super(errorText);

  @override
  bool isValid(String cpf) {
    if (cpf.isEmpty) return true;
    return GetUtils.isCpf(cpf);
  }
}
