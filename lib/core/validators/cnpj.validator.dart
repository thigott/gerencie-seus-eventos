import 'package:form_field_validator/form_field_validator.dart';
import 'package:get/utils.dart';

class CNPJValidator extends TextFieldValidator {
  CNPJValidator({String errorText = "CNPJ inválido"}) : super(errorText);

  @override
  bool isValid(String cnpj) {
    return GetUtils.isCnpj(cnpj);
  }
}
