import 'package:flutter/material.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:intl/intl.dart';

class DateCompareValidator extends TextFieldValidator {
  String errorText;
  DateTime secondDate;
  String comparator;

  DateCompareValidator({
    @required this.comparator,
    @required this.secondDate,
    this.errorText = "Data Inválida",
  }) : super(errorText);

  final formatterDate = new DateFormat('dd/MM/yyyy');

  @override
  bool isValid(String value) {
    if (value.isEmpty) return false;

    if (!RegExp("([0-9]){2}\/([0-9]){2}\/([0-9]){4}").hasMatch(value)) return false;

    var flag = true;
    var date = formatterDate.parse(value);
    var date2 = secondDate;

    switch (comparator) {
      case "=":
        if (!date.isAtSameMomentAs(date2)) {
          errorText = "A data deve ser igual a ${formatterDate.format(date2)}";
          flag = false;
        }
        break;
      case ">":
        if (!date.isAfter(date2)) {
          errorText = "A data deve ser maior que a ${formatterDate.format(date2)}";
          flag = false;
        }
        break;
      case "<":
        if (!date.isBefore(date2)) {
          errorText = "A data deve ser menor que a ${formatterDate.format(date2)}";
          flag = false;
        }
        break;
      case ">=":
        if (!date.isAtSameMomentAs(date2) && !date.isAfter(date2)) {
          errorText = "A data deve ser maior ou igual a ${formatterDate.format(date2)}";
          flag = false;
        }
        break;
      case "<=":
        if (!date.isAtSameMomentAs(date2) && !date.isBefore(date2)) {
          errorText = "A data deve ser menor ou igual a ${formatterDate.format(date2)}";
          flag = false;
        }
        break;
    }

    return flag;
  }
}
