import 'package:form_field_validator/form_field_validator.dart';

class CellphoneValidator extends TextFieldValidator {
  CellphoneValidator({String errorText = "Celular inválido"}) : super(errorText);

  @override
  bool isValid(String phone) {
    if (phone.isEmpty) return true;
    String cleanPhone = phone.replaceAll(RegExp("[() -]"), "");
    return RegExp("(?:[14689][1-9]|2[12478]|3[1234578]|5[1345]|7[134579])9[0-9]{8}").hasMatch(cleanPhone);
  }
}
