abstract class AppRoutes {
  static const HOME = "/home";
  static const SIGN_UP = "/user/sign_up";

  static const MAIN = "/main";

  static const PROVIDERS = "/providers";
}
