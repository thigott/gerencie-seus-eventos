import 'package:gerencie_seu_evento/modules/home/views/home.view.dart';
import 'package:gerencie_seu_evento/modules/main/views/main.view.dart';
import 'package:gerencie_seu_evento/modules/provider/views/providers.view.dart';
import 'package:gerencie_seu_evento/modules/signup/views/signup.view.dart';
import 'package:get/get.dart';
import 'package:get/get_navigation/src/routes/transitions_type.dart';

import 'app_routes.dart';

abstract class PagesRoutes {
  //Initial Route
  static const DEFAULT_TRANSITION = Transition.cupertino;
  static const DEFAULT_TRANSITION_DURATION = Duration(milliseconds: 525);
  static const INITIAL = AppRoutes.HOME;

  static final routes = [
    ..._mainRoutes,
  ];

  static final _mainRoutes = [
    GetPage(name: AppRoutes.HOME, page: () => HomeView()),
    GetPage(name: AppRoutes.SIGN_UP, page: () => SignUpView()),
    GetPage(name: AppRoutes.MAIN, page: () => MainView()),
    GetPage(name: AppRoutes.PROVIDERS, page: () => ItemProviders()),
  ];
}
