class ApiRoutes {
  static String get baseUrl => "https://gerencie-seu-projeto.vercel.app/api";

  static const String USER_REGISTER = "users/inserirUsuario";
  static const String EVENTS = "eventos/listarEventos";
  static const String USER_LOGIN = "users/logar";
  static const String ADD_ACTIVITY = "eventos/inserirAtividadesEvento";
  static const String GET_EVENT_DETAILS = "eventos/buscarDetalhesEvento";
  static const String ADD_FINANTIAL_DETAILS =
      "eventos/inserirDetalhesFinanceiroEvento";

  static const String SYMPLA_GET_MY_EVENTS =
      "https://api.sympla.com.br/public/v3/events";
  static const String SYMPLA_GET_MY_EVENTS_PURCHASE =
      "https://api.sympla.com.br/public/v3/events/event_id/orders";
}
