import 'package:flutter/material.dart';

class AppColors {
  static const Color BLUE36 = Color(0xFF365BC7);
  static const Color BLUEED = Color(0xFFEDF2FF);
  static const Color BLUE54 = Color(0xFFE1F5FE);
  static const Color BLUE12 = Color(0xFF0D47A1);

  static const Color GREEN01 = Color(0xFF018670);
  static const Color GREEN23 = Color(0xFF23A892);
  static const Color GREEN02 = Color(0xFFD8ECD7);
  static const Color GREEN03 = Color(0xFF57CCB9);
  static const Color CONTAINER_GREEN = Color(0xffff3f7e34);

  static const Color TANGERINEFF = Color(0xFFFF9800);
  static const Color TANGERINEED = Color(0xFFED6400);
  static const Color CONTAINER_ORANGE = Color(0xffffEF6236);

  static const Color FOREST = Color(0xFF018670);

  static const Color REDF2 = Color(0xFFF2473F);
  static const Color REDFF = Color(0xFFFF8883);
  static const Color REDF1 = Color(0xFFCC121F);

  static const Color ROUGE = Color(0xFFF2473F);

  static const Color WHITE = Color(0xFFFFFFFF);
  static const Color WHITEFC = Color(0xFFFCFCFC);
  static const Color WHITEFA = Color(0xFFFAFAFA);
  static const Color WHITEF8 = Color(0xFFF8F6F7);

  static const Color GRAYEC = Color(0xFFECEDF1);
  static const Color GRAYED = Color(0xFFECEDF1);
  static const Color GRAYDA = Color(0xFFDADADA);
  static const Color GRAY8A = Color(0xFF8A8A8A);
  static const Color GRAY4F = Color(0xFF4F4F4F);
  static const Color GRAYE0 = Color(0xFFE0E0E0);
  static const Color GRAYBACKGROUND = Color(0xffffdfdfdf);
  static const Color GRAY9E = Color(0xFF9E9E9E);
  static const Color BLACK03 = Color(0xFF191919);

  static const Color YELLOW = Color(0xFFFFC748);

  static const Color SHADOW = Color.fromRGBO(166, 171, 189, 0.53);

  static const Color PRIMARY = BLUE36;
  static const Color SUCCESS = GREEN01;
  static const Color WARNING = TANGERINEED;
  static const Color DANGER = REDF2;
  static const Color DISABLED = GRAYDA;

  static const Color SMOKE = Color(0xff8A8A8A);
  static const Color DIVIDER = Color(0xffffdfdfdf);
  static const Color TITLE_BACKGROUND = Color(0xff607d8b);

  static const Color BUTTON_PURCHASE = Color(0XFF2104CC);

  static const Color LINKS = Color(0xFF00796B);
  static const Color SERENITY_BLUE = Color(0xFFEDF2FF);
}