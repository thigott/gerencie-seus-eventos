import 'package:flutter/material.dart';
import 'package:gerencie_seu_evento/core/styles/appcolors.style.dart';
import 'package:get/get.dart';

class AppStyles {

  static const mainTitle = TextStyle(
    fontSize: 45,
    fontWeight: FontWeight.bold,
    color: Colors.white,
    shadows: [
      Shadow(
        blurRadius: 10.0,
        color: Colors.black,
        offset: Offset(5.0, 5.0),
      ),
    ],
  );

  static const mainSubTitle = TextStyle(
    fontSize: 20,
    fontWeight: FontWeight.normal,
    color: Colors.white,
    shadows: [
      Shadow(
        blurRadius: 10.0,
        color: Colors.black,
        offset: Offset(5.0, 5.0),
      ),
    ],
  );

  static const defaultTitle = TextStyle(
    fontSize: 20,
    fontWeight: FontWeight.bold,
    color: AppColors.BLUE36
  );

  static TextStyle get body1 => Get.context.textTheme.bodyText1;
  static TextStyle get body2 => Get.context.textTheme.bodyText2;
  static TextStyle get caption1 => Get.context.textTheme.caption;
  static TextStyle get caption2 => Get.context.textTheme.caption.copyWith(fontWeight: FontWeight.bold);
  static TextStyle get placeholder => Get.context.textTheme.bodyText1.copyWith(fontStyle: FontStyle.italic, color: AppColors.GRAY8A);

  static const INPUT_PADDING = EdgeInsets.symmetric(horizontal: 10, vertical: 14);
}