class AppIcons {
  static const ICON_ASSETS = "icons";
  static const IMAGES_ASSETS = "images";
  static const LOTTIES_ASSETS = "lotties";

  static const BACKGROUND_SHOW = "$IMAGES_ASSETS/show.jpg";

  static const BILL = "$ICON_ASSETS/conta.svg";
  static const ARROW_DOWN = "$ICON_ASSETS/seta_para_baixo.svg";
  static const DELETE = "$ICON_ASSETS/delete.svg";

  static const ASK_INFORMATION = "$LOTTIES_ASSETS/ask_information.json";

  //Chevrons
  static const CHEVRON_LEFT = "$ICON_ASSETS/chevronleft.svg";
  static const CHEVRON_RIGHT = "$ICON_ASSETS/chevronright.svg";
  static const CHEVRON_UP = "$ICON_ASSETS/chevronup.svg";
  static const CHEVRON_DOWN = "$ICON_ASSETS/chevrondown.svg";

  //LOTIES
  static const SUCCESS = "$LOTTIES_ASSETS/success.json";
  static const EVENT = "$LOTTIES_ASSETS/event.json";
  static const EVENT_WARNING = "$LOTTIES_ASSETS/event_warning.json";
  static const EVENT_SCHEDULE = "$LOTTIES_ASSETS/event_schedule.json";
  static const ADD_TASK = "$LOTTIES_ASSETS/add_task.json";
  static const FAILED = "$LOTTIES_ASSETS/failed.json";
  static const LOADING = "$LOTTIES_ASSETS/loading.json";

  //IMAGES
  static const STORE_IMAGE = "$IMAGES_ASSETS/store.jpg";
}
