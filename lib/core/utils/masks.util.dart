import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';

class Masks {
  //Documents
  static const CPF = "XXX.XXX.XXX-XX";
  static const CNPJ = "XX.XXX.XXX/XXXX-XX";

  //Dates
  static const DATE = "##/##/####";

  //Credit Card
  static const CARD_NUMBER = "#### #### #### ####";
  static const CARD_EXPIRATION = "## / ####";
  static const CARD_SAFE_NUMBER = "####";

  //Address
  static const CEP = "#####-###";

  //Phones
  static const CELLPHONE = "(##) #####-####";
  static const LANDLINE = "(##) ####-####";

  static String unmaskText(String mask, String value) {
    return MaskTextInputFormatter(mask: mask).unmaskText(value);
  }
}
