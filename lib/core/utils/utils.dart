import 'package:flutter/material.dart';
import 'package:gerencie_seu_evento/core/widgets/geral/loading.widget.dart';
import 'package:gerencie_seu_evento/core/widgets/selector/month_selector.selector.dart';
import 'package:gerencie_seu_evento/modules/login/views/login.view.dart';
import 'package:gerencie_seu_evento/modules/main/widgets/finantial/finantial_detail_add_data.widget.dart';
import 'package:gerencie_seu_evento/modules/schedule/models/activity.model.dart';
import 'package:gerencie_seu_evento/modules/schedule/widgets/add_activity.widget.dart';
import 'package:gerencie_seu_evento/modules/signup/views/signup.view.dart';
import 'package:gerencie_seu_evento/modules/signup/widgets/result.widget.dart';
import 'package:get/get.dart';

import '../styles/appcolors.style.dart';

class Utils {
  static showSignUpDialog() => Get.dialog(
        AlertDialog(
          contentPadding: EdgeInsets.zero,
          insetPadding: EdgeInsets.symmetric(horizontal: 100, vertical: 20),
          content: SignUpView(),
        ),
        barrierDismissible: true,
      );

  static showSignUpResult({bool isError = false}) => Get.dialog(
        AlertDialog(
          contentPadding: EdgeInsets.zero,
          content: SignUpResult(isError: isError),
        ),
        barrierDismissible: true,
      );

  static showLoginDialog() => Get.dialog(
        AlertDialog(
          contentPadding: EdgeInsets.zero,
          content: LoginView(),
        ),
        barrierDismissible: true,
      );

  static showAddActivityDialog({Activity activity}) => Get.dialog(
        AlertDialog(
          contentPadding: EdgeInsets.zero,
          content: AddActivity(
            initialActivity: activity,
          ),
        ),
        barrierDismissible: true,
      );

  static showMonthSelectorDialog() => Get.dialog(
        AlertDialog(contentPadding: EdgeInsets.zero, content: MonthSelector()),
        barrierDismissible: true,
      );

  static showAddFinantialDataDialog() => Get.dialog(
        AlertDialog(
          contentPadding: EdgeInsets.zero,
          content: FinantialDetailAddData(),
        ),
        barrierDismissible: true,
      );

  static showLoadingDialog() => Get.dialog(
        AlertDialog(
          contentPadding: EdgeInsets.zero,
          content: LoadingDialog(),
        ),
        barrierDismissible: false,
      );

  static showSuccessSnackBar(BuildContext context) =>
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(
            "Sucesso",
            style: TextStyle(
              color: AppColors.WHITE,
              fontWeight: FontWeight.bold,
              fontSize: 18,
            ),
            textAlign: TextAlign.center,
          ),
          backgroundColor: AppColors.FOREST,
          behavior: SnackBarBehavior.floating,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
        ),
      );

  static showErrorSnackBar(BuildContext context) =>
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text(
            "Ops... tente novamente mais tarde",
            style: TextStyle(
              color: AppColors.WHITE,
              fontWeight: FontWeight.bold,
              fontSize: 18,
            ),
            textAlign: TextAlign.center,
          ),
          backgroundColor: AppColors.REDF1,
          behavior: SnackBarBehavior.floating,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
        ),
      );
}
