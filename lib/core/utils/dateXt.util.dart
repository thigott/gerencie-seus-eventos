import 'package:intl/intl.dart';

String formatDate(DateTime date) {
  try {
    return DateFormat("dd/MM/yyyy").format(date);
  } catch (e) {
    return "";
  }
}

DateTime convertToDate(String input) {
  return DateFormat("dd/MM/yyyy").parse(input);
}
