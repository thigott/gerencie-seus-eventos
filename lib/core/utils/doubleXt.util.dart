import 'package:intl/intl.dart';

String formatAsMoney(double input) {
  final format = NumberFormat("#,##0.00", "pt_BR");
  return "R\$ ${format.format(input)}";
}
