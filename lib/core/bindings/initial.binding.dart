import 'package:gerencie_seu_evento/core/services/api.service.dart';
import 'package:gerencie_seu_evento/modules/login/controllers/login.controller.dart';
import 'package:gerencie_seu_evento/modules/login/repositories/login.repository.dart';
import 'package:gerencie_seu_evento/modules/main/controllers/finantial.controller.dart';
import 'package:gerencie_seu_evento/modules/main/controllers/main.controller.dart';
import 'package:gerencie_seu_evento/modules/main/repositories/finantial.repository.dart';
import 'package:gerencie_seu_evento/modules/provider/controllers/provider.controller.dart';
import 'package:gerencie_seu_evento/modules/schedule/controllers/schedule.controller.dart';
import 'package:gerencie_seu_evento/modules/schedule/repositories/schedule.repository.dart';
import 'package:gerencie_seu_evento/modules/schedule/views/schedule.view.dart';
import 'package:gerencie_seu_evento/modules/signup/controllers/signup.controller.dart';
import 'package:gerencie_seu_evento/modules/signup/repositories/signup.repository.dart';
import 'package:gerencie_seu_evento/modules/signup/stores/signup.store.dart';
import 'package:get/get.dart';
import 'package:get/get_instance/src/bindings_interface.dart';

import '../../modules/dashboards/controllers/dashboard.controller.dart';
import '../../modules/dashboards/repositories/dashboard.repository.dart';

class InitialBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(ApiService());

    Get.lazyPut(() {
      return SignUpController(
        Get.put(
          SignUpStore(
            Get.put(
              SignUpRepository(
                Get.find<ApiService>(),
              ),
            ),
          ),
        ),
      );
    }, fenix: true);

    Get.lazyPut(() {
      return LoginController(
        Get.put(
          LoginRepository(
            Get.find<ApiService>(),
          ),
        ),
      );
    }, fenix: true);

    Get.lazyPut(() {
      return MainController();
    }, fenix: true);

    Get.lazyPut(() {
      return ProviderController();
    }, fenix: true);

    Get.lazyPut(() {
      return ScheduleController(
        ScheduleRepository(
          Get.find<ApiService>(),
        ),
      );
    }, fenix: true);

    Get.lazyPut(() {
      return FinantialController(FinantialRepository(Get.find<ApiService>()));
    }, fenix: true);

    Get.lazyPut(() {
      return DashboardController(DashboardRepository(Get.find<ApiService>()));
    }, fenix: true);
  }
}
