import 'package:gerencie_seu_evento/core/models/apiresponse.model.dart';

abstract class IApi {
  Future<ApiResponseModel> call(
    EApiType type,
    String url, {
    Map<String, dynamic> data,
    Map<String, dynamic> headers,
  });
}

enum EApiType { get, post, put, patch, delete, head }
