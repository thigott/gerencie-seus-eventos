import 'package:flutter/material.dart';
import 'package:gerencie_seu_evento/core/styles/appcolors.style.dart';
import 'package:gerencie_seu_evento/core/styles/appstyles.style.dart';

class CustomButton extends StatelessWidget {
  final String label;
  final double fontSize;
  final Color textColor;
  final bool centerText;
  final bool minSize;
  final CustomButtonType type;
  final Widget leftIcon;
  final Widget rightIcon;
  final Function onTap;
  final bool rounded;
  final bool outlined;
  final bool shadow;
  final Widget loadingIndicator;
  final Color backgroundColor;
  final Color borderColor;
  final double radius;
  final double borderWidth;
  final double horizontalPadding;
  final double verticalPadding;
  final double iconSpacing;
  final TextStyle textStyle;
  final bool enabled;

  const CustomButton({
    Key key,
    this.label = "Label",
    this.textColor = Colors.white,
    this.type,
    this.onTap,
    this.fontSize = 15,
    this.leftIcon,
    this.rightIcon,
    this.rounded = false,
    this.outlined = false,
    this.shadow = false,
    this.borderColor,
    this.borderWidth,
    this.horizontalPadding = 10,
    this.verticalPadding = 13,
    this.backgroundColor = AppColors.PRIMARY,
    this.centerText = false,
    this.minSize = false,
    this.radius = 8,
    this.iconSpacing = 5,
    this.textStyle,
    this.enabled = true,
    this.loadingIndicator,
  })  : this._type = enabled ? type : CustomButtonType.disabled,
        super(key: key);

  final CustomButtonType _type;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: enabled ? onTap : null,
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: horizontalPadding, vertical: verticalPadding),
        decoration: BoxDecoration(
          color: outlined ? Colors.transparent : (_type != null ? _bgColor : backgroundColor),
          borderRadius: BorderRadius.circular(rounded ? 30 : radius),
          border: borderWidth != null
              ? Border.all(color: borderColor != null ? borderColor : _bgColor, width: borderWidth)
              : Border.all(color: Colors.transparent),
          boxShadow: [
            if (shadow)
              BoxShadow(
                color: AppColors.SHADOW,
                spreadRadius: 0,
                blurRadius: 10,
                offset: Offset(10, 10), // changes position of shadow
              ),
          ],
        ),
        child: Row(
          mainAxisAlignment: centerText ? MainAxisAlignment.center : MainAxisAlignment.spaceBetween,
          mainAxisSize: minSize ? MainAxisSize.min : MainAxisSize.max,
          children: [
            if (leftIcon != null) ...{
              leftIcon,
              Container(width: iconSpacing),
            },
            Expanded(
              flex: centerText ? 0 : 1,
              child: loadingIndicator != null
                  ? loadingIndicator
                  : Text(
                "$label",
                textAlign: TextAlign.center,
                style: textStyle != null
                    ? textStyle
                    : AppStyles.body2.copyWith(
                    fontSize: fontSize,
                    fontWeight: FontWeight.bold,
                    color: outlined
                        ? (borderColor != null ? borderColor : _bgColor)
                        : (_type != null ? _textColor : textColor)),
              ),
            ),
            if (rightIcon != null) ...{
              Container(width: iconSpacing),
              rightIcon,
            },
          ],
        ),
      ),
    );
  }

  Color get _bgColor {
    switch (_type) {
      case CustomButtonType.primary:
        return AppColors.PRIMARY;
      case CustomButtonType.success:
        return AppColors.SUCCESS;
      case CustomButtonType.warning:
        return AppColors.WARNING;
      case CustomButtonType.danger:
        return AppColors.DANGER;
      case CustomButtonType.disabled:
        return AppColors.DISABLED;
      default:
        return AppColors.PRIMARY;
    }
  }

  Color get _textColor {
    switch (_type) {
      case CustomButtonType.disabled:
        return AppColors.GRAY8A;
      default:
        return AppColors.WHITE;
    }
  }
}

enum CustomButtonType {
  primary,
  success,
  warning,
  danger,
  disabled,
}