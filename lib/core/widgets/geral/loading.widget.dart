import 'package:flutter/cupertino.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:gerencie_seu_evento/core/styles/appcolors.style.dart';
import 'package:gerencie_seu_evento/core/styles/appicons.style.dart';
import 'package:lottie/lottie.dart';

class LoadingDialog extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        SizedBox(height: 20),
        LottieBuilder.asset(
          AppIcons.LOADING,
          width: 200,
          height: 200,
        ),
        SizedBox(height: 20),
        Text(
          "Carregando...",
          style: TextStyle(fontSize: 14, color: AppColors.BLUE36),
        ),
        SizedBox(height: 20)
      ],
    );
  }
}
