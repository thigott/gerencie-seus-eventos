import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:gerencie_seu_evento/core/styles/appcolors.style.dart';
import 'package:gerencie_seu_evento/core/styles/appicons.style.dart';
import 'package:gerencie_seu_evento/core/styles/appstyles.style.dart';

class CustomDropdownMenu extends StatefulWidget {
  final List<dynamic> items;
  final dynamic selected;
  final String hint;
  final String label;
  final Widget rightIcon;
  final Widget leftIcon;
  final Function(dynamic val) onChanged;

  const CustomDropdownMenu({
    Key key,
    @required this.items,
    @required this.onChanged,
    this.label,
    this.hint,
    this.rightIcon,
    this.leftIcon,
    this.selected,
  }) : super(key: key);

  @override
  _CustomDropdownMenuState createState() => _CustomDropdownMenuState();
}

class _CustomDropdownMenuState extends State<CustomDropdownMenu> {
  final GlobalKey _dropdownKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return _buildTextFieldWithTitle();
  }

  Widget _buildTextFieldWithTitle() {
    return InkWell(
      onTap: openDropdown,
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 6),
        decoration: BoxDecoration(
          border: Border.all(color: AppColors.GRAYEC, width: 2),
          borderRadius: BorderRadius.circular(8),
        ),
        child: Row(
          children: [
            if (widget.leftIcon != null) ...{
              widget.leftIcon,
              Container(width: 13),
            },
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(widget.label, style: AppStyles.caption1.copyWith(color: AppColors.GRAY8A)),
                  DropdownButton(
                    key: _dropdownKey,
                    isDense: true,
                    isExpanded: true,
                    items: widget.items.map((dynamic dropDownStringItem) {
                      return DropdownMenuItem<dynamic>(
                        value: dropDownStringItem,
                        child: Text("$dropDownStringItem"),
                      );
                    }).toList(),
                    hint: widget.hint == null ? null : Text(widget.hint),
                    onChanged: (val) {
                      widget.onChanged(val);
                    },
                    value: widget.selected,
                    style: AppStyles.body1,
                    icon: SvgPicture.asset(
                      AppIcons.CHEVRON_DOWN,
                      color: AppColors.SMOKE,
                    ),
                    underline: Container(),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  void openDropdown() {
    GestureDetector detector;
    void searchForGestureDetector(BuildContext element) {
      element.visitChildElements((element) {
        if (element.widget != null && element.widget is GestureDetector) {
          detector = element.widget;
          return false;
        } else {
          searchForGestureDetector(element);
        }

        return true;
      });
    }

    searchForGestureDetector(_dropdownKey.currentContext);
    assert(detector != null);

    detector.onTap();
  }
}
