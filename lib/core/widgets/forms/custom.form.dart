import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:gerencie_seu_evento/core/styles/appcolors.style.dart';
import 'package:gerencie_seu_evento/core/styles/appicons.style.dart';
import 'package:gerencie_seu_evento/core/styles/appstyles.style.dart';

class CustomTextFormField extends FormField<String> {
  final String hint;
  final String label;
  final String labelExtend;
  final String value;
  final bool enabled;
  final bool disabled;
  final bool obscureText;
  final bool autofocus;
  final bool autocorrect;
  final bool counterText;
  final Widget rightIcon;
  final Widget leftIcon;
  final bool showClearButton;
  final List<TextInputFormatter> inputFormatters;
  final FormFieldValidator<String> validator;
  final AutovalidateMode autovalidateMode;
  final TextInputAction textInputAction;
  final String errorText;
  final int errorMaxLines;
  final int maxLenght;
  final TextInputType keyboardType;
  final TextCapitalization textCapitalization;
  final Function onTap;
  final Function(String val) onSubmitted;
  final Function(String val) onSaved;
  final Function(String val) onChanged;

  CustomTextFormField(
      {Key key,
      this.obscureText = false,
      this.label,
      this.labelExtend,
      this.hint,
      this.value,
      this.enabled = true,
      this.disabled = false,
      this.autofocus = false,
      this.autocorrect,
      this.counterText = false,
      this.showClearButton = false,
      this.inputFormatters,
      this.validator,
      this.rightIcon,
      this.leftIcon,
      this.controller,
      this.focusNode,
      this.autovalidateMode = AutovalidateMode.disabled,
      this.errorText,
      this.errorMaxLines = 3,
      this.maxLenght,
      this.keyboardType,
      this.onSaved,
      this.onTap,
      this.onChanged,
      this.textCapitalization = TextCapitalization.sentences,
      this.textInputAction = TextInputAction.next,
      this.onSubmitted})
      : super(
          autovalidateMode: autovalidateMode,
          validator: validator,
          onSaved: onSaved != null ? (newValue) => onSaved(newValue) : null,
          initialValue: controller != null ? (controller.text ?? '') : (value ?? ''),
          builder: (FormFieldState field) {
            final _CustomTextFormFieldState state = field;

            void onChangedHandler(String value) {
              if (onChanged != null) {
                onChanged(value);
              }
              if (state.hasError) {
                state.validate();
              }
              state.didChange(value);
            }

            void onFocusHandler() {
              if (onTap != null)
                onTap();
              else {
                state._effectiveFocusNode.requestFocus();
                state.didChange(state.value);
              }
            }

            return InkWell(
              onTap: !disabled ? onFocusHandler : null,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    decoration: BoxDecoration(
                      color: disabled ? AppColors.DISABLED : Colors.white,
                      border: Border.all(
                          color: state._effectiveFocusNode.hasFocus
                              ? AppColors.PRIMARY
                              : errorText != null || state.hasError
                                  ? AppColors.DANGER
                                  : AppColors.GRAYEC,
                          width: 2),
                      borderRadius: BorderRadius.circular(8),
                    ),
                    child: Row(
                      children: [
                        if (leftIcon != null) ...{
                          leftIcon,
                          Container(width: 13),
                        },
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Stack(
                                children: [
                                  if (label != null)
                                    AnimatedPositioned(
                                      left: 0,
                                      top: label != null && hint != null || state.value.isNotEmpty
                                          ? 8
                                          : state._effectiveFocusNode.hasFocus
                                              ? 8
                                              : AppStyles.INPUT_PADDING.top,
                                      duration: Duration(milliseconds: 100),
                                      child: state.value.isEmpty ||
                                              state._effectiveFocusNode.hasFocus ||
                                              (label != null && state.value.isNotEmpty)
                                          ? RichText(
                                              text: TextSpan(children: [
                                              TextSpan(
                                                text: label,
                                                style: label != null && hint != null || state.value.isNotEmpty
                                                    ? AppStyles.caption1.copyWith(
                                                        color: state._effectiveFocusNode.hasFocus ? AppColors.PRIMARY : AppColors.GRAY8A)
                                                    : state._effectiveFocusNode.hasFocus
                                                        ? AppStyles.caption1.copyWith(color: AppColors.PRIMARY)
                                                        : AppStyles.placeholder,
                                              ),
                                              TextSpan(
                                                text: labelExtend != null ? " $labelExtend " : "",
                                                style: label != null && hint != null || state.value.isNotEmpty
                                                    ? AppStyles.caption1.copyWith(
                                                        color: state._effectiveFocusNode.hasFocus ? AppColors.BLUE36 : AppColors.BLUE36)
                                                    : state._effectiveFocusNode.hasFocus
                                                        ? AppStyles.caption1.copyWith(color: AppColors.PRIMARY)
                                                        : AppStyles.placeholder,
                                              )
                                            ]))
                                          : Container(),
                                    ),
                                  AnimatedPadding(
                                    padding: label != null && hint != null || state.value.isNotEmpty
                                        ? EdgeInsets.only(bottom: 6, top: 22)
                                        : state._effectiveFocusNode.hasFocus
                                            ? (label == null && state.value.isNotEmpty)
                                                ? EdgeInsets.symmetric(vertical: 14)
                                                : EdgeInsets.only(bottom: 6, top: 22)
                                            : EdgeInsets.symmetric(vertical: 14),
                                    duration: Duration(milliseconds: 100),
                                    child: Padding(
                                      padding: const EdgeInsets.only(top: 8.0),
                                      child: TextField(
                                        controller: state._effectiveController,
                                        enabled: enabled,
                                        onTap: onFocusHandler,
                                        keyboardType: keyboardType,
                                        textCapitalization: textCapitalization,
                                        onChanged: onChangedHandler,
                                        focusNode: state._effectiveFocusNode,
                                        textInputAction: textInputAction,
                                        autofocus: autofocus,
                                        autocorrect: autocorrect ?? keyboardType == TextInputType.emailAddress ? false : true,
                                        decoration: InputDecoration(
                                          hintText: hint,
                                          isDense: true,
                                          border: OutlineInputBorder(borderSide: BorderSide.none),
                                          enabledBorder: OutlineInputBorder(borderSide: BorderSide.none),
                                          disabledBorder: OutlineInputBorder(borderSide: BorderSide.none),
                                          contentPadding: EdgeInsets.zero,
                                          errorMaxLines: errorMaxLines,
                                          hintStyle: AppStyles.body1.copyWith(color: AppColors.DISABLED),
                                          counterText: counterText ? null : "",
                                        ),
                                        maxLength: maxLenght,
                                        onSubmitted: onSubmitted,
                                        style: AppStyles.body1,
                                        obscureText: obscureText,
                                        inputFormatters: inputFormatters,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                        if (rightIcon != null || showClearButton) ...{
                          Container(width: 13),
                          if (rightIcon != null) ...{
                            rightIcon,
                          } else if (showClearButton && state._effectiveController.text.isNotEmpty) ...{
                            GestureDetector(
                                onTap: () => state._effectiveController.clear(),
                                child: SvgPicture.asset(
                                  AppIcons.DELETE,
                                  width: 15,
                                  height: 15,
                                  color: AppColors.DISABLED,
                                ))
                          }
                        },
                      ],
                    ),
                  ),
                  if (errorText != null || state.hasError) ...{
                    Container(height: 5),
                    Text(
                      state.errorText ?? errorText,
                      style: AppStyles.caption1.copyWith(color: AppColors.DANGER),
                    ),
                  }
                ],
              ),
            );
          },
        );

  final TextEditingController controller;

  final FocusNode focusNode;

  @override
  _CustomTextFormFieldState createState() => _CustomTextFormFieldState();
}

class _CustomTextFormFieldState extends FormFieldState<String> {
  TextEditingController _controller;
  FocusNode _focusNode;

  TextEditingController get _effectiveController => widget.controller ?? _controller;

  FocusNode get _effectiveFocusNode => widget.focusNode ?? _focusNode;

  @override
  CustomTextFormField get widget => super.widget;

  @override
  void initState() {
    super.initState();
    if (widget.controller == null) {
      _controller = TextEditingController(text: widget.initialValue);
    } else {
      widget.controller.addListener(_handleControllerChanged);
    }

    if (widget.focusNode == null) _focusNode = FocusNode();
    _effectiveFocusNode.addListener(_handleFocusNodeChanged);
  }

  @override
  void didUpdateWidget(CustomTextFormField oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (widget.controller != oldWidget.controller) {
      oldWidget.controller?.removeListener(_handleControllerChanged);
      widget.controller?.addListener(_handleControllerChanged);

      if (oldWidget.controller != null && widget.controller == null)
        _controller = TextEditingController.fromValue(oldWidget.controller.value);
      if (widget.controller != null) {
        setValue(widget.controller.text);
        if (oldWidget.controller == null) _controller = null;
      }
    } else {
      _effectiveController.text = widget.initialValue;
      _effectiveController.value = _effectiveController.value.copyWith(
        text: _effectiveController.text,
        selection: TextSelection(
          baseOffset: _effectiveController.text.length,
          extentOffset: _effectiveController.text.length,
        ),
        composing: TextRange.empty,
      );
    }
  }

  @override
  void dispose() {
    widget.controller?.removeListener(_handleControllerChanged);
    super.dispose();
  }

  @override
  void reset() {
    super.reset();
    setState(() {
      _effectiveController.text = widget.initialValue;
    });
  }

  void _handleFocusNodeChanged() {
    didChange(_effectiveController.text);
    _effectiveController.value = _effectiveController.value.copyWith(
      text: _effectiveController.text,
      selection: TextSelection(
        baseOffset: _effectiveController.text.length,
        extentOffset: _effectiveController.text.length,
      ),
      composing: TextRange.empty,
    );
  }

  void _handleControllerChanged() {
    if (_effectiveController.text != value) didChange(_effectiveController.text);
  }
}
