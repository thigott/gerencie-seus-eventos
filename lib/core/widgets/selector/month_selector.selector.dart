import 'package:flutter/material.dart';
import 'package:gerencie_seu_evento/core/styles/appcolors.style.dart';
import 'package:gerencie_seu_evento/core/utils/dateXt.util.dart';
import 'package:gerencie_seu_evento/core/widgets/buttons/custom.button.dart';
import 'package:gerencie_seu_evento/modules/main/controllers/finantial.controller.dart';
import 'package:get/get.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';

class MonthSelector extends GetView<FinantialController> {
  @override
  Widget build(BuildContext context) {
    var initDate = "".obs;
    var endDate = "".obs;
    PickerDateRange pickerDateRange;

    if (controller.selectedFilterDate.isNotEmpty) {
      initDate.value = controller.selectedFilterDate[0];
      endDate.value = controller.selectedFilterDate[1];

      pickerDateRange = PickerDateRange(
          convertToDate(initDate.value), convertToDate(endDate.value));
    }

    return Container(
      width: MediaQuery.of(context).size.height * 0.8,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Padding(
            padding: const EdgeInsets.all(1.5),
            child: Container(
              padding: EdgeInsets.all(8),
              decoration: BoxDecoration(
                  color: AppColors.BLUE36,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(8),
                      topRight: Radius.circular(8))),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Obx(
                    () => Text(
                      "Data inicial: \n\n${initDate.value.isEmpty ? "-" : initDate.value}",
                      style: TextStyle(color: Colors.white, fontSize: 14),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  SizedBox(width: 30),
                  Container(height: 30, width: 1, color: Colors.white),
                  SizedBox(width: 30),
                  Obx(
                    () => Text(
                      "Data final: \n\n${endDate.value.isEmpty ? "-" : endDate.value}",
                      style: TextStyle(color: Colors.white, fontSize: 14),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ],
              ),
            ),
          ),
          SizedBox(height: 20),
          SfDateRangePicker(
            initialDisplayDate: initDate.value.isEmpty ? null : convertToDate(initDate.value),
            selectionMode: DateRangePickerSelectionMode.range,
            initialSelectedRange: pickerDateRange,
            onSelectionChanged: (DateRangePickerSelectionChangedArgs args) {
              PickerDateRange dates = args.value as PickerDateRange;
              initDate.value = formatDate(dates.startDate);
              endDate.value = formatDate(dates.endDate);
            },
          ),
          Obx(
            () => Padding(
              padding: const EdgeInsets.all(16.0).copyWith(top: 0),
              child: CustomButton(
                label: "Selecionar",
                enabled: initDate.value.isNotEmpty && endDate.value.isNotEmpty,
                onTap: () {
                  controller.filterFinantialDetails(
                      startDate: convertToDate(initDate.value),
                      endDate: convertToDate(endDate.value));
                  Get.back();
                },
              ),
            ),
          )
        ],
      ),
    );
  }
}
