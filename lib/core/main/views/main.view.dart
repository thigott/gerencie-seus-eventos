import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:gerencie_seu_evento/core/bindings/initial.binding.dart';
import 'package:gerencie_seu_evento/core/routes/page_routes.dart';
import 'package:gerencie_seu_evento/core/styles/apptheme.style.dart';
import 'package:get/get_navigation/src/root/get_material_app.dart';

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Gerencie seu Evento!',
      theme: AppTheme.themeData,
      debugShowCheckedModeBanner: false,
      getPages: PagesRoutes.routes,
      defaultTransition: PagesRoutes.DEFAULT_TRANSITION,
      transitionDuration: PagesRoutes.DEFAULT_TRANSITION_DURATION,
      initialRoute: PagesRoutes.INITIAL,
      initialBinding: InitialBinding(),
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: [
        const Locale('pt', ''),
      ]
    );
  }
}
