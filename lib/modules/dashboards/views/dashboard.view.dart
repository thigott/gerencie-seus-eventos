import 'package:flutter/cupertino.dart';
import 'package:gerencie_seu_evento/modules/dashboards/controllers/dashboard.controller.dart';
import 'package:get/get.dart';

class DashBoardView extends GetView<DashboardController> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        GestureDetector(
          behavior: HitTestBehavior.translucent,
          onTap: () => controller.getMyEvents(),
          child: Obx(() => controller.event.value.id == null
              ? Text("Sympla")
              : Text("Sympla 2")),
        ),
      ],
    );
  }
}
