class SymplaEventPurchaseModel {
  String id;
  int eventId;
  String orderDate;
  String orderStatus;
  String updatedDate;
  String approvedDate;
  String discountCode;
  String transactionType;
  double orderTotalSalePrice;
  String buyerFirstName;
  String buyerLastName;
  String buyerEmail;
  InvoiceInfo invoiceInfo;

  SymplaEventPurchaseModel(
      {this.id,
      this.eventId,
      this.orderDate,
      this.orderStatus,
      this.updatedDate,
      this.approvedDate,
      this.discountCode,
      this.transactionType,
      this.orderTotalSalePrice,
      this.buyerFirstName,
      this.buyerLastName,
      this.buyerEmail,
      this.invoiceInfo});

  SymplaEventPurchaseModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    eventId = json['event_id'];
    orderDate = json['order_date'];
    orderStatus = json['order_status'];
    updatedDate = json['updated_date'];
    approvedDate = json['approved_date'];
    discountCode = json['discount_code'];
    transactionType = json['transaction_type'];
    orderTotalSalePrice = json['order_total_sale_price'];
    buyerFirstName = json['buyer_first_name'];
    buyerLastName = json['buyer_last_name'];
    buyerEmail = json['buyer_email'];
    invoiceInfo = json['invoice_info'] != null
        ? new InvoiceInfo.fromJson(json['invoice_info'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['event_id'] = this.eventId;
    data['order_date'] = this.orderDate;
    data['order_status'] = this.orderStatus;
    data['updated_date'] = this.updatedDate;
    data['approved_date'] = this.approvedDate;
    data['discount_code'] = this.discountCode;
    data['transaction_type'] = this.transactionType;
    data['order_total_sale_price'] = this.orderTotalSalePrice;
    data['buyer_first_name'] = this.buyerFirstName;
    data['buyer_last_name'] = this.buyerLastName;
    data['buyer_email'] = this.buyerEmail;
    if (this.invoiceInfo != null) {
      data['invoice_info'] = this.invoiceInfo.toJson();
    }
    return data;
  }
}

class InvoiceInfo {
  String docType;
  String docNumber;
  String clientName;
  String addressZipCode;
  String addressStreet;
  String addressStreetNumber;
  String addressStreet2;
  String addressNeighborhood;
  String addressCity;
  String addressState;
  bool mei;

  InvoiceInfo(
      {this.docType,
      this.docNumber,
      this.clientName,
      this.addressZipCode,
      this.addressStreet,
      this.addressStreetNumber,
      this.addressStreet2,
      this.addressNeighborhood,
      this.addressCity,
      this.addressState,
      this.mei});

  InvoiceInfo.fromJson(Map<String, dynamic> json) {
    docType = json['doc_type'];
    docNumber = json['doc_number'];
    clientName = json['client_name'];
    addressZipCode = json['address_zip_code'];
    addressStreet = json['address_street'];
    addressStreetNumber = json['address_street_number'];
    addressStreet2 = json['address_street2'];
    addressNeighborhood = json['address_neighborhood'];
    addressCity = json['address_city'];
    addressState = json['address_state'];
    mei = json['mei'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['doc_type'] = this.docType;
    data['doc_number'] = this.docNumber;
    data['client_name'] = this.clientName;
    data['address_zip_code'] = this.addressZipCode;
    data['address_street'] = this.addressStreet;
    data['address_street_number'] = this.addressStreetNumber;
    data['address_street2'] = this.addressStreet2;
    data['address_neighborhood'] = this.addressNeighborhood;
    data['address_city'] = this.addressCity;
    data['address_state'] = this.addressState;
    data['mei'] = this.mei;
    return data;
  }
}
