import 'package:gerencie_seu_evento/modules/dashboards/models/sympla_event_purchase.model.dart';

class SymplaEventModel {
  SymplaEventModel(
      {this.id,
      this.startDate,
      this.endDate,
      this.name,
      this.detail,
      this.privateEvent,
      this.published,
      this.cancelled,
      this.image,
      this.address,
      this.host,
      this.url});

  int id;
  String startDate;
  String endDate;
  String name;
  String detail;
  int privateEvent;
  int published;
  int cancelled;
  String image;
  SymplaEventAddressModel address;
  SymplaEventHostModel host;
  String url;
  var purchaseData = <SymplaEventPurchaseModel>[];

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "start_date": startDate == null ? null : startDate,
        "end_date": endDate == null ? null : endDate,
        "name": name == null ? null : name,
        "detail": detail == null ? null : detail,
        "private_event": privateEvent == null ? null : privateEvent,
        "published": published == null ? null : published,
        "cancelled": cancelled == null ? null : cancelled,
        "image": image == null ? null : image,
        "url": url == null ? null : url
      };

  static SymplaEventModel fromJson(Map<String, dynamic> data) {
    return SymplaEventModel(
        id: data["id"],
        startDate: data["start_date"],
        endDate: data["end_date"],
        name: data["name"],
        detail: data["detail"],
        privateEvent: data["private_event"],
        published: data["published"],
        cancelled: data["cancelled"],
        image: data["image"],
        address: SymplaEventAddressModel.fromJson(data["address"]),
        host: SymplaEventHostModel.fromJson(data["host"]),
        url: data["url"]);
  }
}

class SymplaEventAddressModel {
  SymplaEventAddressModel({
    this.name,
    this.address,
    this.number,
    this.neighborhood,
    this.city,
    this.state,
    this.zipCode,
    this.country,
  });

  String name;
  String address;
  String number;
  String neighborhood;
  String city;
  String state;
  String zipCode;
  String country;

  Map<String, dynamic> toJson() => {
        "name": name == null ? null : name,
        "address": address == null ? null : address,
        "address_num": number == null ? null : number,
        "neighborhood": neighborhood == null ? null : neighborhood,
        "city": city == null ? null : city,
        "state": state == null ? null : state,
        "zip_code": zipCode == null ? null : zipCode,
        "country": country == null ? null : country
      };

  static SymplaEventAddressModel fromJson(Map<String, dynamic> data) {
    return SymplaEventAddressModel(
      name: data["name"],
      address: data["address"],
      number: data["address_num"],
      neighborhood: data["neighborhood"],
      city: data["city"],
      state: data["state"],
      zipCode: data["cancelled"],
      country: data["country"],
    );
  }
}

class SymplaEventHostModel {
  SymplaEventHostModel({
    this.name,
    this.description,
  });

  String name;
  String description;

  Map<String, dynamic> toJson() => {
        "name": name == null ? null : name,
        "description": description == null ? null : description,
      };

  static SymplaEventHostModel fromJson(Map<String, dynamic> data) {
    return SymplaEventHostModel(
      name: data["name"],
      description: data["description"],
    );
  }
}
