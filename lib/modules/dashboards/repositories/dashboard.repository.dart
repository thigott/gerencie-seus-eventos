import 'package:gerencie_seu_evento/modules/dashboards/models/sympla_event.model.dart';
import 'package:gerencie_seu_evento/modules/dashboards/models/sympla_event_purchase.model.dart';

import '../../../core/interfaces/api.interface.dart';
import '../../../core/routes/api_routes.dart';

class DashboardRepository {
  DashboardRepository(this._iApi);
  IApi _iApi;

  Future<SymplaEventModel> getMyEvents() async {
    try {
      var result = await _iApi.call(
          EApiType.get, ApiRoutes.SYMPLA_GET_MY_EVENTS, headers: {
        "s_token":
            "fe619730c6a97345297bec156f3fa9e4476ec5407709df807c8935c400768ca7"
      });

      var eventList = result.data["data"];

      return SymplaEventModel.fromJson(eventList[0]);
    } catch (e) {
      return null;
    }
  }

  Future<List<SymplaEventPurchaseModel>> getMyEventsPurchase(
      int eventId) async {
    try {
      var result = await _iApi.call(
        EApiType.get,
        ApiRoutes.SYMPLA_GET_MY_EVENTS_PURCHASE.replaceAll(
          "event_id",
          eventId.toString(),
        ),
        headers: {
          "s_token":
              "fe619730c6a97345297bec156f3fa9e4476ec5407709df807c8935c400768ca7"
        },
      );

      List<SymplaEventPurchaseModel> returnList = [];
      var purchaseList = result.data["data"] as List;
      purchaseList.forEach((element) {
        returnList.add(SymplaEventPurchaseModel.fromJson(element));
      });

      return returnList;
    } catch (e) {
      return null;
    }
  }
}
