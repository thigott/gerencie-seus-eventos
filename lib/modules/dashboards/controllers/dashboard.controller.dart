import 'package:gerencie_seu_evento/modules/dashboards/models/sympla_event.model.dart';
import 'package:gerencie_seu_evento/modules/dashboards/repositories/dashboard.repository.dart';
import 'package:get/get.dart';

class DashboardController extends GetxController {
  DashboardController(this.repository);
  DashboardRepository repository;

  Rx<SymplaEventModel> event = SymplaEventModel().obs;

  getMyEvents() async {
    await repository.getMyEvents().then((value) {
      getMyEventsPurchase(value);
    }).catchError((error) => null);
  }

  getMyEventsPurchase(SymplaEventModel event) async {
    await repository.getMyEventsPurchase(event.id).then((data) {
      event.purchaseData = data;
      this.event.value = event;
    }).catchError((error) => null);
  }
}
