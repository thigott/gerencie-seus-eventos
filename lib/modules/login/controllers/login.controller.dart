import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:gerencie_seu_evento/core/routes/app_routes.dart';
import 'package:gerencie_seu_evento/core/utils/utils.dart';
import 'package:gerencie_seu_evento/modules/login/repositories/login.repository.dart';
import 'package:get/get.dart';

class LoginController extends GetxController {
  LoginController(this.loginRepository);
  LoginRepository loginRepository;
  final isLoading = false.obs;

  String email = "";
  String password = "";

  final GlobalKey<FormState> loginForm = GlobalKey();
  final isError = false.obs;

  makeLogin() async {
    if (loginForm.currentState.validate()) {
      isLoading.value = true;

      loginRepository.login(email: email, password: password).then((user) {
        isLoading.value = false;
        if (user != null) {
          isError.value = false;
          Get.toNamed(AppRoutes.MAIN,
              parameters: {"user": jsonEncode(user.toJson())});
          Utils.showLoadingDialog();
        } else {
          isError.value = true;
        }
      }).catchError((error) {
        isLoading.value = false;
        isError.value = true;
      });
    }
  }
}
