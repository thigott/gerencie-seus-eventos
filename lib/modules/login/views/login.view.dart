import 'package:flutter/material.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:gerencie_seu_evento/core/styles/appcolors.style.dart';
import 'package:gerencie_seu_evento/core/validators/default_required.validator.dart';
import 'package:gerencie_seu_evento/core/widgets/buttons/custom.button.dart';
import 'package:gerencie_seu_evento/core/widgets/forms/custom.form.dart';
import 'package:gerencie_seu_evento/modules/login/controllers/login.controller.dart';
import 'package:get/get.dart';

class LoginView extends GetView<LoginController> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(20.0),
      child: Form(
        key: controller.loginForm,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              constraints: BoxConstraints(minWidth: 500),
              child: CustomTextFormField(
                label: "E-mail",
                keyboardType: TextInputType.name,
                onChanged: (value) {
                  controller.email = value;
                  controller.isError.value = false;
                },
                validator: MultiValidator([
                  DefaultRequiredValidator(),
                  EmailValidator(errorText: "E-mail inválido!"),
                ]),
              ),
            ),
            SizedBox(height: 20),
            CustomTextFormField(
              label: "Senha",
              keyboardType: TextInputType.visiblePassword,
              obscureText: true,
              onChanged: (value) {
                controller.password = value;
                controller.isError.value = false;
              },
              validator: MultiValidator([
                DefaultRequiredValidator(),
              ]),
            ),
            Obx(() => (controller.isError.value == true)
                ? Padding(
                    padding: const EdgeInsets.only(top: 20),
                    child: Text(
                      "Usuário ou senha inválidos",
                      style: TextStyle(color: AppColors.REDF1, fontSize: 14),
                    ),
                  )
                : Container()),
            SizedBox(height: 40),
            Obx(
              () => CustomButton(
                label: "Entrar",
                backgroundColor: AppColors.BLUE36,
                onTap: () => controller.makeLogin(),
                enabled: controller.isLoading.value != true,
                loadingIndicator: controller.isLoading.value != true
                    ? null
                    : Center(
                        child: SizedBox(
                          width: 20,
                          height: 20,
                          child: CircularProgressIndicator(
                            valueColor:
                                AlwaysStoppedAnimation<Color>(Colors.white),
                          ),
                        ),
                      ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
