import 'dart:convert';

import 'package:gerencie_seu_evento/core/interfaces/api.interface.dart';
import 'package:gerencie_seu_evento/core/routes/api_routes.dart';
import 'package:gerencie_seu_evento/modules/signup/models/user.model.dart';

class LoginRepository {
  LoginRepository(this._api);
  IApi _api;

  Future<UserModel> login({String email, String password}) async {
    try {
      var result = await _api.call(
        EApiType.post,
        ApiRoutes.USER_LOGIN,
        data: {"email": email.toString(), "senha": password.toString()},
      );

      var user = UserModel.fromJson(result.data["response"]);
      return user;
    } catch (e) {
      return null;
    }
  }
}
