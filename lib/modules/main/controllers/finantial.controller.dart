import 'dart:convert';
import 'dart:html';
import 'dart:js';

import 'package:excel/excel.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:gerencie_seu_evento/core/styles/appcolors.style.dart';
import 'package:gerencie_seu_evento/core/utils/dateXt.util.dart';
import 'package:gerencie_seu_evento/core/utils/utils.dart';
import 'package:gerencie_seu_evento/modules/main/models/finantial_detail.model.dart';
import 'package:gerencie_seu_evento/modules/main/repositories/finantial.repository.dart';
import 'package:gerencie_seu_evento/modules/main/widgets/finantial/finantial_extract_detail.widget.dart';
import 'package:gerencie_seu_evento/modules/main/widgets/finantial/finantial_extract_movement_detail.widget.dart';
import 'package:gerencie_seu_evento/modules/schedule/controllers/schedule.controller.dart';
import 'package:gerencie_seu_evento/modules/signup/models/user.model.dart';
import 'package:get/get.dart';

class FinantialController extends GetxController {
  FinantialController(this.repository) {
    updateFinantialDetails();
  }

  UserModel user;

  FinantialRepository repository;
  ScheduleController scheduleController = Get.find();

  RxList<dynamic> finantialDetails = [].obs;
  RxList<dynamic> finantialDetailsFiltered = [].obs;
  List<double> graphValues = [];
  RxList<dynamic> selectedFilterDate = [].obs;

  final GlobalKey<FormState> addFinantialDataForm = GlobalKey();

  final isFinantialDetailBeingAdded = false.obs;

  get isLoading => isFinantialDetailBeingAdded.value;

  updateFinantialDetails() async {
    user = UserModel.fromJson(jsonDecode(Get.parameters["user"]));
    await repository.getFinantialDetails(user.email).then((value) {
      finantialDetails.addAll(value);
      finantialDetailsFiltered.addAll(value);
    }).catchError((error) {});
  }

  selectCSVFile() async {
    FilePickerResult csvFile = await FilePicker.platform.pickFiles(
      allowedExtensions: ["xlsx"],
      type: FileType.custom,
      allowMultiple: false,
    );

    if (csvFile != null) {
      finantialDetails.clear();
      var bytes = csvFile.files.single.bytes;
      var excel = Excel.decodeBytes(bytes);

      excel.tables.forEach((key, value) {
        getFinantialDetails(value);
      });

      finantialDetailsFiltered.clear();
      finantialDetailsFiltered.addAll(finantialDetails);

      finantialDetails.refresh();
      updateMenuSelector(viewSelector.first);
    }
  }

  int findInputPosition(Sheet sheet, String valueSearch) {
    var valuePosition = -1;
    sheet.rows[0].asMap().forEach((index, element) {
      if (element.value.contains(valueSearch)) {
        valuePosition = index;
      }
    });

    return valuePosition;
  }

  getFinantialDetails(Sheet sheet) {
    sheet.rows.asMap().forEach(
      (index, data) {
        if (index != 0) {
          var model = FinantialDetailModel();
          model.type = sheet.sheetName;

          getFinantialValue(
            data,
            findInputPosition(sheet, "Valor"),
            model,
          );
          getFinantialDescription(
            data,
            findInputPosition(sheet, "Catégoria"),
            model,
          );
          getFinantialResponsible(
            data,
            findInputPosition(sheet, "Responsável"),
            model,
          );
          getFinantialDate(
            data,
            findInputPosition(sheet, "Data"),
            model,
          );
          getFinantialObservation(
            data,
            findInputPosition(sheet, "Observação"),
            model,
          );

          if (model.isDataValid()) {
            finantialDetails.add(model);
          }
        }
      },
    );
  }

  getFinantialValue(
    List<Data> data,
    int valuePosition,
    FinantialDetailModel model,
  ) {
    if (valuePosition == -1) return;
    var element = data[valuePosition];
    if (element != null) model.value = double.parse(element.value.toString());
  }

  getFinantialDescription(
    List<Data> data,
    int valuePosition,
    FinantialDetailModel model,
  ) {
    if (valuePosition == -1) return;
    var element = data[valuePosition];
    if (element != null) model.description = element.value.toString();
  }

  getFinantialResponsible(
    List<Data> data,
    int valuePosition,
    FinantialDetailModel model,
  ) {
    if (valuePosition == -1) return;
    var element = data[valuePosition];
    if (element != null) model.responsible = element.value.toString();
  }

  getFinantialDate(
    List<Data> data,
    int valuePosition,
    FinantialDetailModel model,
  ) {
    if (valuePosition == -1) return;
    var element = data[valuePosition];
    if (element != null) model.date = DateTime.parse(element.value.toString());
  }

  getFinantialObservation(
    List<Data> data,
    int valuePosition,
    FinantialDetailModel model,
  ) {
    if (valuePosition == -1) return;
    var element = data[valuePosition];
    if (element != null) model.observation = element.value.toString();
  }

  downloadExcelSheet() {
    AnchorElement anchorElement =
        new AnchorElement(href: "documents/import.xlsx");
    anchorElement.download = "PlanilhaModelo";
    anchorElement.click();
  }

  final viewSelector = ["Movimento Financeiro", "Extrato"].obs;

  var viewSelected = "Movimento Financeiro".obs;

  getViewSelected() => viewSelected.value;

  updateMenuSelector(String menuItem) {
    viewSelected.value = menuItem;
    viewSelected.refresh();
  }

  Widget getActualView(String menuSelected) {
    switch (menuSelected) {
      case "Extrato":
        return FinantialExtractDetail();

      default:
        return FinantialExtractMovementDetail();
    }
  }

  var totalRevenue = 0.0;

  double calculateTotalRevenue() {
    double revenue = 0.0;

    finantialDetailsFiltered.forEach((element) {
      if (element.type != "Saídas") {
        revenue = revenue + element.value;
      }
    });

    totalRevenue = revenue;
    return revenue;
  }

  double futureRevenue = 0.0;

  double calculateFutureRevenue() {
    double revenue = 0.0;

    finantialDetailsFiltered.forEach((element) {
      if (element.type != "Saídas" && DateTime.now().isBefore(element.date)) {
        revenue = revenue + element.value;
      }
    });

    futureRevenue = revenue;
    return revenue;
  }

  double calculateActualRevenue() {
    return calculateTotalRevenue() - calculateFutureRevenue();
  }

  double totalExpenses = 0.0;

  double calculateTotalExpenses() {
    double expenses = 0.0;

    finantialDetailsFiltered.forEach((element) {
      if (element.type == "Saídas") {
        expenses = expenses + element.value;
      }
    });

    totalExpenses = expenses;
    return expenses;
  }

  double futureExpenses = 0.0;

  double calculateFutureExpenses() {
    double expenses = 0.0;

    finantialDetailsFiltered.forEach((element) {
      if (element.type == "Saídas" && DateTime.now().isBefore(element.date)) {
        expenses = expenses + element.value;
      }
    });

    futureExpenses = expenses;
    return expenses;
  }

  double calculateActualExpenses() {
    return calculateTotalExpenses() - calculateFutureExpenses();
  }

  double calculateProfit() {
    return calculateTotalRevenue() - calculateTotalExpenses();
  }

  double calculateActualProfit() {
    return calculateActualRevenue() - calculateActualExpenses();
  }

  List<double> calculateValuesForGraph() {
    graphValues.clear();
    graphValues.add(calculateActualRevenue());
    graphValues.add(calculateActualExpenses());
    graphValues.add(calculateFutureRevenue());
    graphValues.add(calculateFutureExpenses());
    graphValues.add(calculateActualProfit());
    graphValues.add(calculateProfit());

    return graphValues;
  }

  List<String> getGraphLabels() {
    return [
      "Receita Atual",
      "Gasto Atual",
      "Receita Futura",
      "Gasto Futuro",
      "Lucro Atual",
      "Lucro Futuro",
    ];
  }

  Color getGraphColor(double value) {
    var position = graphValues.indexOf(value);

    if (position == 1 || position == 3) {
      return AppColors.REDF1;
    } else if (position == 0 || position == 2) {
      return AppColors.FOREST;
    } else {
      return AppColors.BLUE36;
    }
  }

  RxList<String> months = [
    "Janeiro",
    "Fevereiro",
    "Março",
    "Abril",
    "Maio",
    "Junho",
    "Julho",
    "Agosto",
    "Setembro",
    "Outubro",
    "Novembro",
    "Dezembro",
  ].obs;

  filterFinantialDetails({DateTime startDate, DateTime endDate}) {
    updateMenuSelector(viewSelector.first);
    finantialDetailsFiltered.clear();

    selectedFilterDate.clear();
    selectedFilterDate.addAll([formatDate(startDate), formatDate(endDate)]);

    List<dynamic> listFiltered = finantialDetails
        .where((element) =>
            (element as FinantialDetailModel).date.isAfter(startDate) &&
            (element as FinantialDetailModel)
                .date
                .isBefore(endDate.add(Duration(days: 1))))
        .toList();

    finantialDetailsFiltered.addAll(listFiltered);
  }

  addFinantialDetailData({FinantialDetailModel item}) {
    item.prepareData();
    finantialDetails.add(item);
    finantialDetails.refresh();

    finantialDetailsFiltered.clear();
    finantialDetailsFiltered.addAll(finantialDetails);

    finantialDetails.refresh();
    updateMenuSelector(viewSelector.first);

    Get.back();
  }

  String getMonthsFilteredLabel() {
    if (selectedFilterDate.isEmpty) return "";

    var label = "*Período: ${selectedFilterDate[0]} - ${selectedFilterDate[1]}";

    return label;
  }

  saveFinantialDetails(BuildContext context) async {
    isFinantialDetailBeingAdded.value = true;
    repository.addFinantialDetail(finantialDetails, user.email).then((value) {
      isFinantialDetailBeingAdded.value = false;
      Utils.showSuccessSnackBar(context);
    }).catchError((error) {
      isFinantialDetailBeingAdded.value = false;
      Utils.showErrorSnackBar(context);
    });
  }
}
