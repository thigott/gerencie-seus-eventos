import 'package:flutter/cupertino.dart';
import 'package:gerencie_seu_evento/core/styles/appicons.style.dart';
import 'package:gerencie_seu_evento/modules/main/controllers/finantial.controller.dart';
import 'package:gerencie_seu_evento/modules/main/models/event.model.dart';
import 'package:gerencie_seu_evento/modules/main/views/finantial_detail.view.dart';
import 'package:gerencie_seu_evento/modules/main/views/finantial_summary.view.dart';
import 'package:gerencie_seu_evento/modules/schedule/controllers/schedule.controller.dart';
import 'package:gerencie_seu_evento/modules/schedule/views/schedule.view.dart';
import 'package:gerencie_seu_evento/modules/schedule/widgets/schedule_activity.widget.dart';
import 'package:get/get.dart';

import '../../dashboards/views/dashboard.view.dart';
import '../../signup/models/user.model.dart';

class MainController extends GetxController {
  ScheduleController scheduleController = Get.find();
  FinantialController finantialController = Get.find();

  UserModel get userModel => scheduleController.user;

  final eventList = [
    Event("Evento 1", AppIcons.EVENT),
    Event("Evento 2", AppIcons.EVENT_SCHEDULE),
    Event("Evento 3", AppIcons.EVENT_WARNING),
    Event("Evento 4", AppIcons.EVENT)
  ];

  final menuList = [
    "Dados Cadastrais",
    "Meus eventos",
    "Encontre um fornecedor",
    "Histórico"
  ];

  final menuSelector =
      ["Resumo dos Eventos", "Financeiro", "Agenda", "Dashboards"].obs;

  var menuSelected = "Resumo dos Eventos".obs;

  getMenuSelected() => menuSelected.value;

  updateMenuSelector(String menuItem) {
    menuSelected.value = menuItem;
    menuSelector.refresh();
  }

  Widget getActualView(String menuSelected) {
    switch (menuSelected) {
      case "Agenda":
        return ScheduleView();

      case "Financeiro":
        return FinantialDetail();

      case "Dashboards":
        return DashBoardView();

      default:
        return FinantialSummary();
    }
  }
}
