import 'package:flutter/material.dart';
import 'package:gerencie_seu_evento/core/styles/appcolors.style.dart';
import 'package:gerencie_seu_evento/modules/main/widgets/menuitem.widget.dart';
import 'package:get/get.dart';

class MenuApp extends StatelessWidget {
  MenuApp({this.itemSelected = "Início"});
  final String itemSelected;
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(20),
      child: Container(
        constraints: BoxConstraints(minWidth: 200),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10),
        ),
        padding: EdgeInsets.all(20),
        child: Column(
          children: [
            Text(
              "Menu",
              style: TextStyle(
                color: AppColors.BLUE36,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(height: 20),
            GestureDetector(
              behavior: HitTestBehavior.translucent,
              onTap: () => Get.back(),
              child: MenuEventItem(
                itemMenu: "Início",
                isSelected: itemSelected == "Início",
              ),
            ),
            SizedBox(height: 20),
            GestureDetector(
              behavior: HitTestBehavior.translucent,
              onTap: () => {Get.back(), Get.back()},
              child: MenuEventItem(
                itemMenu: "Sair",
                isSelected: false,
                isDisabled: false,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
