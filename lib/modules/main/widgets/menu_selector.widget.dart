import 'package:flutter/material.dart';
import 'package:gerencie_seu_evento/core/styles/appcolors.style.dart';
import 'package:gerencie_seu_evento/modules/main/controllers/main.controller.dart';
import 'package:get/get.dart';

class MenuSelector extends GetView<MainController> {
  @override
  Widget build(BuildContext context) {
    return Obx(
      () => ListView.separated(
        itemBuilder: (_, index) {
          final menuItem = controller.menuSelector[index];
          return MouseRegion(
            cursor: SystemMouseCursors.click,
            child: GestureDetector(
              onTap: () => {
                if (index < 4) {controller.updateMenuSelector(menuItem)}
              },
              behavior: HitTestBehavior.translucent,
              child: Text(
                menuItem,
                style: TextStyle(
                  fontSize: 21,
                  color: menuItem == controller.getMenuSelected()
                      ? AppColors.BLACK03
                      : AppColors.GRAY8A,
                  fontWeight: menuItem == controller.getMenuSelected()
                      ? FontWeight.bold
                      : FontWeight.normal,
                ),
              ),
            ),
          );
        },
        separatorBuilder: (_, __) => Container(width: 20),
        itemCount: controller.menuSelector.length,
        shrinkWrap: true,
        scrollDirection: Axis.horizontal,
      ),
    );
  }
}
