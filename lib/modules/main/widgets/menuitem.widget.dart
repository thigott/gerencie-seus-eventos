import 'package:flutter/material.dart';
import 'package:gerencie_seu_evento/core/styles/appcolors.style.dart';

class MenuEventItem extends StatelessWidget {
  MenuEventItem({
    this.itemMenu,
    this.isSelected = false,
    this.isDisabled = false,
  });

  final String itemMenu;
  final bool isSelected;
  final bool isDisabled;

  @override
  Widget build(BuildContext context) {
    return MouseRegion(
      cursor: SystemMouseCursors.click,
      child: Container(
        width: 180,
        height: 40,
        decoration: BoxDecoration(
            color: isDisabled
                ? AppColors.GRAYEC
                : (isSelected ? AppColors.BLUE36 : Colors.white),
            border: Border.all(
              color: isDisabled ? AppColors.GRAYEC : AppColors.BLUE36,
              width: 1,
            ),
            borderRadius: BorderRadius.circular(20)),
        child: Center(
          child: Text(
            itemMenu,
            style: TextStyle(
              color: isSelected ? Colors.white : Colors.black87,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      ),
    );
  }
}
