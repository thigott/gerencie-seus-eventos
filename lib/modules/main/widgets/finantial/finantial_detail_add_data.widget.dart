import 'package:flutter/material.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:gerencie_seu_evento/core/styles/appicons.style.dart';
import 'package:gerencie_seu_evento/core/utils/masks.util.dart';
import 'package:gerencie_seu_evento/core/validators/date.validator.dart';
import 'package:gerencie_seu_evento/core/validators/default_required.validator.dart';
import 'package:gerencie_seu_evento/core/widgets/buttons/custom.button.dart';
import 'package:gerencie_seu_evento/core/widgets/forms/custom.form.dart';
import 'package:gerencie_seu_evento/modules/main/controllers/finantial.controller.dart';
import 'package:gerencie_seu_evento/modules/main/models/finantial_detail.model.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';

class FinantialDetailAddData extends GetView<FinantialController> {
  @override
  Widget build(BuildContext context) {
    FinantialDetailModel model = FinantialDetailModel();

    return GestureDetector(
      onTap: () {
        FocusManager.instance.primaryFocus.unfocus();
      },
      child: SingleChildScrollView(
        padding: EdgeInsets.all(20),
        child: Column(
          children: [
            Center(
              child: Lottie.asset(
                AppIcons.ADD_TASK,
                width: 300,
                height: 250,
              ),
            ),
            Container(
              width: 900,
              child: Card(
                elevation: 10,
                color: Colors.white,
                child: Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(top: 20.0),
                        child: Form(
                          key: controller.addFinantialDataForm,
                          child: Column(
                            children: [
                              CustomTextFormField(
                                value: model.description,
                                label: "Descrição",
                                onChanged: (value) => model.description = value,
                                validator: MultiValidator(
                                  [DefaultRequiredValidator()],
                                ),
                              ),
                              SizedBox(height: 10),
                              CustomTextFormField(
                                value: model.inputDate,
                                label: "Data: (dd/MM/YYYY)",
                                onChanged: (value) => model.inputDate = value,
                                inputFormatters: [
                                  MaskTextInputFormatter(mask: Masks.DATE)
                                ],
                                validator: MultiValidator(
                                  [
                                    DefaultRequiredValidator(),
                                    CustomDateValidator(),
                                  ],
                                ),
                              ),
                              SizedBox(height: 10),
                              CustomTextFormField(
                                value: model.responsible,
                                label: "Responsável (opcional)",
                                onChanged: (value) => model.responsible = value,
                              ),
                              SizedBox(height: 10),
                              CustomTextFormField(
                                value: model.observation,
                                label: "Observações (opcional)",
                                onChanged: (value) => model.observation = value,
                              ),
                              SizedBox(height: 10),
                              CustomTextFormField(
                                value: model.inputValue,
                                label: "Valor",
                                keyboardType: TextInputType.number,
                                controller: MoneyMaskedTextController(
                                    decimalSeparator: ",",
                                    thousandSeparator: "."),
                                onChanged: (value) =>
                                    model.inputValue = "R\$ $value",
                                validator: DefaultRequiredValidator(),
                              ),
                              SizedBox(height: 10),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(height: 5),
                      Padding(
                        padding: const EdgeInsets.only(left: 4.0),
                        child: Center(
                          child: Row(
                            children: [
                              Text(
                                "Tipo: ",
                              ),
                              incomesType(model),
                              outcomesType(model),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(height: 10),
                      Padding(
                        padding: const EdgeInsets.all(20.0).copyWith(bottom: 0),
                        child: Container(
                          width: 200,
                          child: CustomButton(
                            label: "CADASTRAR",
                            onTap: () => controller.addFinantialDetailData(
                              item: model,
                            ),
                            enabled: true,
                            loadingIndicator: null,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget incomesType(FinantialDetailModel model) {
    return Obx(
      () => Row(
        children: [
          Radio(
            value: "Entradas",
            groupValue: model.inputType.value,
            onChanged: (state) => model.inputType.value = "Entradas",
          ),
          Text("Entradas"),
        ],
      ),
    );
  }

  Widget outcomesType(FinantialDetailModel model) {
    return Obx(
      () => Row(
        children: [
          Radio(
            value: "Saídas",
            groupValue: model.inputType.value,
            onChanged: (state) => model.inputType.value = "Saídas",
          ),
          Text("Saídas"),
        ],
      ),
    );
  }
}
