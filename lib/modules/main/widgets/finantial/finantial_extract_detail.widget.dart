import 'package:flutter/material.dart';
import 'package:gerencie_seu_evento/core/styles/appcolors.style.dart';
import 'package:gerencie_seu_evento/modules/main/controllers/finantial.controller.dart';
import 'package:get/get.dart';

import 'finantial_detail_header.widget.dart';
import 'finantial_detail_item.widget.dart';

class FinantialExtractDetail extends GetView<FinantialController> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(12.0),
      decoration: BoxDecoration(
        border: Border.all(
          color: AppColors.BLUE36,
          width: 1.0,
        ),
        borderRadius: BorderRadius.circular(20.0),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          FinantialDetailHeader(),
          SizedBox(
            height: 15,
          ),
          Container(
            height: 1,
            color: AppColors.GRAY4F,
          ),
          SizedBox(
            height: 15,
          ),
          Expanded(
            child: Obx(
              () => ListView.separated(
                itemBuilder: (_, index) {
                  var item = controller.finantialDetailsFiltered[index];
                  return FinantialDetailItem(
                    data: item,
                  );
                },
                separatorBuilder: (_, __) => Container(
                  height: 20,
                ),
                itemCount: controller.finantialDetailsFiltered.length,
                shrinkWrap: true,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
