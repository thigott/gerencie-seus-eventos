import 'package:flutter/material.dart';
import 'package:gerencie_seu_evento/core/styles/appcolors.style.dart';
import 'package:gerencie_seu_evento/core/utils/dateXt.util.dart';
import 'package:gerencie_seu_evento/core/utils/doubleXt.util.dart';
import 'package:gerencie_seu_evento/modules/main/models/finantial_detail.model.dart';

class FinantialDetailItem extends StatelessWidget {
  FinantialDetailItem({this.data});
  final FinantialDetailModel data;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Container(
          width: 120,
          child: Text(
            data.description,
            textAlign: TextAlign.center,
            style: TextStyle(
                fontSize: 17.0,
                fontWeight: FontWeight.bold,
                color: data.type == "Saídas" ? AppColors.REDF1 : AppColors.FOREST),
          ),
        ),
        SizedBox(
          width: 20,
        ),
        Container(
          width: 120,
          child: Text(
            formatDate(data.date),
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 17.0,
            ),
          ),
        ),
        SizedBox(
          width: 20,
        ),
        Container(
          width: 120,
          child: Text(
            formatAsMoney(data.value),
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 17.0,
            ),
          ),
        ),
        SizedBox(
          width: 20,
        ),
        Container(
          width: 120,
          child: Text(
            data.responsible,
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 17.0,
            ),
          ),
        ),
        SizedBox(
          width: 20,
        ),
        Container(
          width: 120,
          child: Text(
            data.observation,
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 17.0,
            ),
          ),
        )
      ],
    );
  }
}
