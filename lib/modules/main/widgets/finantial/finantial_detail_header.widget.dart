import 'package:flutter/material.dart';
import 'package:gerencie_seu_evento/modules/main/controllers/finantial.controller.dart';
import 'package:get/get.dart';

class FinantialDetailHeader extends GetView<FinantialController> {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Container(
          width: 120,
          child: Text(
            "Ação",
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 17.0,
              fontWeight: FontWeight.bold,
              color: Colors.black,
            ),
          ),
        ),
        SizedBox(
          width: 20,
        ),
        Container(
          width: 120,
          child: Text(
            "Data",
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 17.0,
              fontWeight: FontWeight.bold,
              color: Colors.black,
            ),
          ),
        ),
        SizedBox(
          width: 20,
        ),
        Container(
          width: 120,
          child: Text(
            "Valor",
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 17.0,
              fontWeight: FontWeight.bold,
              color: Colors.black,
            ),
          ),
        ),
        SizedBox(
          width: 20,
        ),
        Container(
          width: 120,
          child: Text(
            "Responsável",
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 17.0,
              fontWeight: FontWeight.bold,
              color: Colors.black,
            ),
          ),
        ),
        SizedBox(
          width: 20,
        ),
        Container(
          width: 120,
          child: Text(
            "Observações",
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 17.0,
              fontWeight: FontWeight.bold,
              color: Colors.black,
            ),
          ),
        ),
        Spacer(),
        Padding(
          padding: const EdgeInsets.only(right: 18),
          child: Text(controller.getMonthsFilteredLabel()),
        )
      ],
    );
  }
}
