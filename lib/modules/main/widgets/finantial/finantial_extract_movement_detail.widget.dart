import 'package:flutter/material.dart';
import 'package:gerencie_seu_evento/core/styles/appcolors.style.dart';
import 'package:gerencie_seu_evento/core/utils/doubleXt.util.dart';
import 'package:gerencie_seu_evento/modules/main/controllers/finantial.controller.dart';
import 'package:get/get.dart';
import 'package:chart_components/chart_components.dart';

class FinantialExtractMovementDetail extends GetView<FinantialController> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        decoration: BoxDecoration(
            border: Border(
                top: BorderSide(
          color: AppColors.BLUE36,
          width: 1,
        ))),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(height: 20),
            Text(
              controller.getMonthsFilteredLabel(),
              style: TextStyle(
                fontSize: 15.0
              ),
            ),
            SizedBox(height: 20),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                eventExtractMovementDetail(
                  title: "Receita do evento",
                  value: controller.calculateActualRevenue(),
                  isRevenue: true,
                ),
                eventExtractMovementDetail(
                  title: "Receita futura",
                  value: controller.calculateFutureRevenue(),
                  isRevenue: true,
                ),
                eventExtractMovementDetail(
                  title: "Valor gasto",
                  value: controller.calculateTotalExpenses(),
                  isRevenue: false,
                ),
                eventExtractMovementDetail(
                  title: "Saída futura",
                  value: controller.calculateFutureExpenses(),
                  isRevenue: false,
                ),
              ],
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                eventExtractMovementDetail(
                  title: "Lucro líquido atual",
                  value: controller.calculateActualProfit(),
                  isRevenue: true,
                ),
                eventExtractMovementDetail(
                  title: "Lucro líquido previsto",
                  value: controller.calculateProfit(),
                  isRevenue: true,
                ),
              ],
            ),
            SizedBox(height: 40),
            Container(
              width: 1000,
              height: 300,
              child: BarChart(
                data: controller.calculateValuesForGraph(),
                labels: controller.getGraphLabels(),
                getColor: controller.getGraphColor,
                animationDuration: Duration(milliseconds: 2400),
                barSeparation: 36,
                displayValue: true,
                lineGridColor: null,
                barWidth: 125,
                roundValuesOnText: true,
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget eventExtractMovementDetail({
    String title,
    double value,
    bool isRevenue,
  }) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        height: 100,
        width: 180,
        decoration: BoxDecoration(
            border: Border.all(
              color: AppColors.BLACK03,
              width: 1,
            ),
            borderRadius: BorderRadius.circular(16.0)),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                title,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: AppColors.BLUE36,
                  fontSize: 18,
                ),
                textAlign: TextAlign.center,
              ),
              SizedBox(height: 8),
              Text(
                formatAsMoney(value),
                style: TextStyle(
                  color: isRevenue
                      ? (value.isNegative ? AppColors.REDF1 : AppColors.FOREST)
                      : AppColors.REDF1,
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
