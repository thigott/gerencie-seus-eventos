import 'package:flutter/material.dart';
import 'package:gerencie_seu_evento/core/styles/appcolors.style.dart';
import 'package:gerencie_seu_evento/modules/main/controllers/main.controller.dart';
import 'package:gerencie_seu_evento/modules/schedule/models/activity.model.dart';
import 'package:get/get.dart';

class EventActivity extends GetView<MainController> {
  EventActivity({this.activity});

  final Activity activity;

  @override
  Widget build(BuildContext context) {
    return MouseRegion(
      cursor: SystemMouseCursors.click,
      child: GestureDetector(
        onTap: () => controller.updateMenuSelector(controller.menuSelector[2]),
        child: Container(
          decoration: BoxDecoration(
            color: activity.type == ActivityType.ENDED ? AppColors.GREEN02 : Colors.white,
            borderRadius: BorderRadius.circular(10),
          ),
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      height: 16,
                      width: 20,
                      color: activity.determinePriorityColor(),
                    ),
                    SizedBox(width: 10),
                    Text(
                      activity.title,
                      style: TextStyle(fontSize: 16),
                    ),
                  ],
                ),
                SizedBox(height: 10),
                Text(
                  "Data: ${activity.endDate}",
                  style: TextStyle(fontSize: 14),
                ),
                activity.value.isNotEmpty
                    ? Padding(
                        padding: const EdgeInsets.only(top: 5),
                        child: RichText(
                          text: TextSpan(
                            children: [
                              TextSpan(
                                text: "Valor: ",
                              ),
                              TextSpan(
                                text: activity.value,
                                style: TextStyle(
                                  color: AppColors.FOREST,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ],
                          ),
                        ),
                      )
                    : Container(),
                Spacer(),
                Text(
                  "Status: ${activity.determineStatus()}",
                  style: TextStyle(
                    fontSize: 12,
                    fontWeight: FontWeight.bold,
                    color: activity.type == ActivityType.ENDED
                        ? AppColors.BLUE36
                        : AppColors.BLACK03,
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
