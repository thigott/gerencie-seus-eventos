import 'package:flutter/material.dart';
import 'package:gerencie_seu_evento/core/styles/appcolors.style.dart';
import 'package:gerencie_seu_evento/core/styles/appicons.style.dart';
import 'package:gerencie_seu_evento/core/utils/doubleXt.util.dart';
import 'package:gerencie_seu_evento/core/utils/utils.dart';
import 'package:gerencie_seu_evento/modules/main/controllers/finantial.controller.dart';
import 'package:gerencie_seu_evento/modules/main/widgets/activity.widget.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';
import 'package:pie_chart/pie_chart.dart';

class EventItem extends GetView<FinantialController> {
  EventItem({
    this.eventName,
    this.lottieFile = AppIcons.EVENT,
  });

  final String eventName;
  final String lottieFile;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(borderRadius: BorderRadius.circular(10)),
      child: Card(
        color: Colors.white,
        elevation: 10,
        child: Padding(
          padding: const EdgeInsets.all(20.0),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                eventInformation(),
                finantialSummary(),
                activitiesSummary(context),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget eventInformation() {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Lottie.asset(lottieFile,
            width: 30, height: 30, frameRate: FrameRate.max),
        SizedBox(width: 20),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              eventName,
              style: TextStyle(
                color: AppColors.BLUE36,
                fontWeight: FontWeight.bold,
                fontSize: 18,
              ),
            ),
            Text(
              "354 pessoas",
              style: TextStyle(
                color: AppColors.BLUE36,
                fontSize: 14,
              ),
            ),
          ],
        ),
      ],
    );
  }

  Widget finantialSummary() {
    return Obx(
      () => Row(
        children: [
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              border: Border.all(
                width: 1.0,
                color: AppColors.BLUE36,
              ),
            ),
            child: Padding(
              padding: const EdgeInsets.all(8.0).copyWith(right: 120),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    "Resumo financeiro",
                    style: TextStyle(fontSize: 21),
                  ),
                  SizedBox(height: 32),
                  Text(
                    "Receita bruta: ${formatAsMoney(controller.calculateTotalRevenue())}",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: AppColors.FOREST,
                      fontSize: 18,
                    ),
                  ),
                  SizedBox(height: 8),
                  Text(
                    "Lucro líquido: ${formatAsMoney(controller.calculateProfit())}",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: AppColors.FOREST,
                      fontSize: 18,
                    ),
                  ),
                  SizedBox(height: 8),
                  Text(
                    "Gasto planejado: ${formatAsMoney(controller.calculateFutureExpenses())}",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: AppColors.REDF1,
                      fontSize: 18,
                    ),
                  ),
                  SizedBox(height: 8),
                  Text(
                    "Gasto real: ${formatAsMoney(controller.calculateActualExpenses())}",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: AppColors.REDF1,
                      fontSize: 18,
                    ),
                  ),
                  controller.getMonthsFilteredLabel().isNotEmpty
                      ? Column(
                          children: [
                            SizedBox(height: 20),
                            Text(controller.getMonthsFilteredLabel()),
                          ],
                        )
                      : Container(),
                ],
              ),
            ),
          ),
          SizedBox(width: 30),
          Container(
            height: 320,
            width: 360,
            child: PieChart(
              dataMap: {
                "Receita Bruta": controller.calculateTotalRevenue(),
                "Lucro líquido": controller.calculateProfit(),
                "Gasto planejado": controller.calculateTotalExpenses() -
                    controller.calculateActualExpenses(),
                "Gasto real": controller.calculateActualExpenses(),
              },
              chartValuesOptions: ChartValuesOptions(
                showChartValueBackground: true,
                showChartValues: true,
                showChartValuesInPercentage: true,
                showChartValuesOutside: true,
                decimalPlaces: 1,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget activitiesSummary(BuildContext context) {
    return Row(
      children: [
        Container(
          constraints:
              BoxConstraints(maxWidth: MediaQuery.of(context).size.width * 0.6),
          decoration: BoxDecoration(
            color: AppColors.BLUE36,
            borderRadius: BorderRadius.circular(20),
          ),
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Resumo atividades",
                  style: TextStyle(
                    fontSize: 18,
                    color: Colors.white,
                  ),
                ),
                SizedBox(height: 10),
                Container(
                  height: 100,
                  child: Obx(
                    () => controller.scheduleController.activities.isNotEmpty
                        ? ListView.separated(
                            itemBuilder: (_, index) => EventActivity(
                              activity: controller
                                  .scheduleController.activities[index],
                            ),
                            separatorBuilder: (_, __) => Container(width: 10),
                            itemCount: controller.scheduleController
                                .getActivitiesSize(),
                            scrollDirection: Axis.horizontal,
                            shrinkWrap: true,
                          )
                        : Padding(
                            padding: const EdgeInsets.only(top: 30.0),
                            child: Text(
                              "Nenhuma atividade cadastrada",
                              style: TextStyle(color: Colors.white),
                            ),
                          ),
                  ),
                ),
              ],
            ),
          ),
        ),
        (false)
            ? Column(
                children: [
                  SizedBox(
                    width: 30,
                  ),
                  MouseRegion(
                    cursor: SystemMouseCursors.click,
                    child: GestureDetector(
                      onTap: () => {Utils.showAddActivityDialog()},
                      child: Card(
                        elevation: 10,
                        color: Colors.white,
                        child: Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Column(
                            children: [
                              Icon(
                                Icons.add_circle_rounded,
                                color: AppColors.BLUE36,
                                size: 30.0,
                              ),
                              SizedBox(height: 4),
                              Text(
                                "Nova atividade",
                                style: TextStyle(
                                  fontSize: 11,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              )
            : Container()
      ],
    );
  }

  Widget ticketSummary() {
    return Row(
      children: [
        Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            color: Colors.white,
            border: Border.all(
              color: AppColors.BLUE36,
              width: 1,
            ),
          ),
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Resumo das vendas",
                  style: TextStyle(fontSize: 18),
                ),
                SizedBox(height: 10),
                Text(
                  "Quantidade total vendido: 1000 (unidades)",
                ),
                SizedBox(height: 5),
                Text(
                  "Quantidade vendido no lote 1: 200 (unidades)",
                ),
                SizedBox(height: 5),
                Text(
                  "Quantidade vendido no lote 2: 500 (unidades)",
                ),
                SizedBox(height: 5),
                Text(
                  "Quantidade vendido no lote 3: 300 (unidades)",
                ),
                SizedBox(height: 5),
              ],
            ),
          ),
        ),
        SizedBox(width: 30),
        Container(
          height: 270,
          width: 270,
          child: PieChart(
            dataMap: {
              "Vendas 1 lote": 200,
              "Vendas 2 lote": 500,
              "Vendas 3 lote": 300,
            },
            chartType: ChartType.ring,
            chartValuesOptions: ChartValuesOptions(
              showChartValueBackground: true,
              showChartValues: true,
              showChartValuesInPercentage: true,
              showChartValuesOutside: false,
              decimalPlaces: 1,
            ),
          ),
        ),
      ],
    );
  }
}
