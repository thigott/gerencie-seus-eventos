import 'dart:convert';

import 'package:gerencie_seu_evento/core/interfaces/api.interface.dart';
import 'package:gerencie_seu_evento/core/routes/api_routes.dart';
import 'package:gerencie_seu_evento/modules/main/models/finantial_detail.model.dart';
import 'package:gerencie_seu_evento/modules/schedule/models/activity.model.dart';
import 'package:get/get.dart';

class FinantialRepository {
  FinantialRepository(this._iApi);
  IApi _iApi;

  Future<bool> addFinantialDetail(
      RxList<dynamic> finantialDetails, String email) async {
    try {
      var result = await _iApi
          .call(EApiType.post, ApiRoutes.ADD_FINANTIAL_DETAILS, data: {
        "nome": "Evento $email",
        "detalhes": jsonEncode(finantialDetails.map((e) => e.toJson()).toList())
      });

      return result.data["response"];
    } catch (e) {
      return null;
    }
  }

  Future<List<FinantialDetailModel>> getFinantialDetails(String email) async {
    try {
      var result = await _iApi.call(
        EApiType.post,
        ApiRoutes.GET_EVENT_DETAILS,
        data: {"nome": "Evento $email"},
      );

      List<FinantialDetailModel> response = [];
      (result.data["response"]["detalhes"] as List).forEach((element) {
        response.add(FinantialDetailModel.fromJson(element));
      });

      return response;
    } catch (e) {
      return null;
    }
  }
}
