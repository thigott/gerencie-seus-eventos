import 'package:gerencie_seu_evento/core/utils/dateXt.util.dart';
import 'package:get/get.dart';

class FinantialDetailModel {
  FinantialDetailModel({
    this.type,
    this.description,
    this.date,
    this.value,
    this.responsible,
    this.observation,
  });

  String type;
  String description;
  DateTime date;
  double value;
  String responsible;
  String observation;

  String inputDate = "";
  String inputValue = "";
  final inputType = "Entradas".obs;

  isDataValid() {
    return value != null && description != null;
  }

  prepareData() {
    type = inputType.value;
    date = convertToDate(inputDate);
    String inputValueFormatted = inputValue
        .replaceAll("R\$", "")
        .replaceAll(".", "")
        .replaceAll(",", ".");
    value = double.parse(inputValueFormatted);

    if (observation == null) {
      observation = "-";
    }

    if (responsible == null) {
      responsible = "-";
    }
  }

  Map<String, dynamic> toJson() => {
        "type": type == null ? null : type,
        "description": description == null ? null : description,
        "date": date == null ? null : formatDate(date),
        "value": value == null ? null : value,
        "responsible": responsible == null ? null : responsible,
        "observation": observation == null ? null : observation,
      };

  static FinantialDetailModel fromJson(Map<String, dynamic> data) {
    return FinantialDetailModel(
        type: data["type"],
        description: data["description"],
        date: convertToDate(data["date"]),
        value: data["value"],
        responsible: data["responsible"] != null ? data["responsible"] : "-",
        observation: data["observation"] != null ? data["observation"] : "-");
  }
}
