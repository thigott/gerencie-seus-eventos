import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:gerencie_seu_evento/core/styles/appcolors.style.dart';
import 'package:gerencie_seu_evento/core/utils/utils.dart';
import 'package:gerencie_seu_evento/core/widgets/buttons/custom.button.dart';
import 'package:gerencie_seu_evento/modules/main/controllers/finantial.controller.dart';
import 'package:get/get.dart';

class FinantialDetail extends GetView<FinantialController> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(
          height: 10,
        ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            importSheetButton(),
            SizedBox(width: 20),
            importManualDataButton()
          ],
        ),
        SizedBox(
          height: 20,
        ),
        MouseRegion(
          cursor: SystemMouseCursors.click,
          child: GestureDetector(
            onTap: () => controller.downloadExcelSheet(),
            child: Text(
              "Clique aqui para baixar a planilha modelo",
              style: TextStyle(
                color: AppColors.LINKS,
                fontSize: 16,
                decoration: TextDecoration.underline,
              ),
            ),
          ),
        ),
        SizedBox(
          height: 20,
        ),
        Obx(
          () => Container(
            width: double.infinity,
            height: 30,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                viewSelector(controller.viewSelected.value),
                filterButton(),
              ],
            ),
            padding: const EdgeInsets.only(left: 32),
          ),
        ),
        Expanded(
          flex: 2,
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Obx(
              () => controller.getActualView(controller.viewSelected.value),
            ),
          ),
        ),
        SizedBox(height: 20),
        Obx(
          () => Container(
            width: 200,
            child: CustomButton(
              label: "Salvar",
              onTap: () => controller.saveFinantialDetails(context),
              enabled: controller.isLoading != true,
              loadingIndicator: controller.isLoading != true
                  ? null
                  : Center(
                      child: SizedBox(
                        width: 20,
                        height: 20,
                        child: CircularProgressIndicator(
                          valueColor: AlwaysStoppedAnimation<Color>(
                            Colors.white,
                          ),
                        ),
                      ),
                    ),
            ),
          ),
        ),
      ],
    );
  }

  Widget importSheetButton() {
    return Container(
      width: 240,
      height: 40,
      child: MaterialButton(
        color: AppColors.BLUE36,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20.0),
        ),
        onPressed: () => controller.selectCSVFile(),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Icon(
              Icons.upload,
              color: Colors.white,
            ),
            SizedBox(width: 10),
            Text(
              "Importar planilha",
              style: TextStyle(
                color: AppColors.WHITE,
                fontWeight: FontWeight.bold,
                fontSize: 18,
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget importManualDataButton() {
    return Container(
      width: 240,
      height: 40,
      child: MaterialButton(
        color: AppColors.BLUE36,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20.0),
        ),
        onPressed: () => Utils.showAddFinantialDataDialog(),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Icon(
              Icons.upload,
              color: Colors.white,
            ),
            SizedBox(width: 10),
            Text(
              "Importar manualmente",
              style: TextStyle(
                color: AppColors.WHITE,
                fontWeight: FontWeight.bold,
                fontSize: 18,
              ),
            ),
          ],
        ),
      ),
    );
  }

  viewSelector(String viewSelected) {
    return ListView.separated(
      itemBuilder: (_, index) {
        final menuItem = controller.viewSelector[index];
        return MouseRegion(
          cursor: SystemMouseCursors.click,
          child: GestureDetector(
            onTap: () => controller.updateMenuSelector(menuItem),
            behavior: HitTestBehavior.translucent,
            child: Text(
              menuItem,
              style: TextStyle(
                fontSize: 21,
                color: menuItem == viewSelected
                    ? AppColors.BLACK03
                    : AppColors.GRAY8A,
                fontWeight: menuItem == viewSelected
                    ? FontWeight.bold
                    : FontWeight.normal,
              ),
            ),
          ),
        );
      },
      separatorBuilder: (_, __) => Container(width: 20),
      itemCount: controller.viewSelector.length,
      shrinkWrap: true,
      scrollDirection: Axis.horizontal,
    );
  }

  Widget filterButton() {
    return Padding(
      padding: const EdgeInsets.only(right: 24.0),
      child: Container(
        width: 120,
        child: CustomButton(
          verticalPadding: 8,
          label: "Filtrar",
          fontSize: 12,
          outlined: true,
          leftIcon: Icon(
            Icons.keyboard_arrow_down,
            color: AppColors.BLUE36,
            size: 15,
          ),
          iconSpacing: 10,
          borderWidth: 1,
          borderColor: AppColors.BLUE36,
          onTap: () => Utils.showMonthSelectorDialog(),
          enabled: true,
        ),
      ),
    );
  }
}
