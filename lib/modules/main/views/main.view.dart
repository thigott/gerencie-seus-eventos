import 'package:flutter/material.dart';
import 'package:gerencie_seu_evento/core/styles/appcolors.style.dart';
import 'package:gerencie_seu_evento/modules/main/controllers/main.controller.dart';
import 'package:gerencie_seu_evento/modules/main/widgets/menu.widget.dart';
import 'package:gerencie_seu_evento/modules/main/widgets/menu_selector.widget.dart';
import 'package:get/get.dart';

class MainView extends GetView<MainController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.BLUEED,
      body: WillPopScope(
        onWillPop: () async {
          return false;
        },
        child: Row(
          children: [
            MenuApp(),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Padding(
                    padding: EdgeInsets.all(20.0).copyWith(bottom: 0, top: 24),
                    child: Container(
                      height: 24,
                      child: MenuSelector(),
                    ),
                  ),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: Container(
                        decoration: BoxDecoration(
                          color: AppColors.WHITE,
                          borderRadius: BorderRadius.circular(10),
                        ),
                        padding: EdgeInsets.all(10),
                        child: Obx(
                          () => controller
                              .getActualView(controller.menuSelected.value),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
