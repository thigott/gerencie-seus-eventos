import 'package:flutter/material.dart';
import 'package:gerencie_seu_evento/core/styles/appcolors.style.dart';
import 'package:gerencie_seu_evento/modules/main/controllers/main.controller.dart';
import 'package:gerencie_seu_evento/modules/main/widgets/event.widget.dart';
import 'package:get/get.dart';

class FinantialSummary extends GetView<MainController> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Padding(
          padding: const EdgeInsets.all(10.0),
          child: Text(
            'Olá ${controller.userModel.name}, confira o resumo do seus últimos eventos',
            style: TextStyle(
              color: AppColors.BLUE36,
              fontWeight: FontWeight.bold,
              fontSize: 21,
            ),
          ),
        ),
        Expanded(
          child: PageView.builder(
            itemCount: 1,
            itemBuilder: (context, itemIndex) {
              final event = controller.eventList[itemIndex];
              return EventItem(
                eventName: event.name,
                lottieFile: event.lottieFile,
              );
            },
          ),
        ),
      ],
    );
  }
}
