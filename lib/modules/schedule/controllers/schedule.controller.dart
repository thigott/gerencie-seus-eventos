import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:gerencie_seu_evento/core/utils/utils.dart';
import 'package:gerencie_seu_evento/modules/schedule/models/activity.model.dart';
import 'package:gerencie_seu_evento/modules/schedule/repositories/schedule.repository.dart';
import 'package:gerencie_seu_evento/modules/signup/models/user.model.dart';
import 'package:get/get.dart';

class ScheduleController {
  ScheduleController(this.scheduleRepository) {
    updateActivities();
  }

  ScheduleRepository scheduleRepository;

  final GlobalKey<FormState> activityForm = GlobalKey();

  bool isFirstLoad = true;

  final newActivities = [].obs;
  final executingActivities = [].obs;
  final endedActivities = [].obs;

  RxList<dynamic> activities = [].obs;

  final isActivityBeingAdded = false.obs;

  get isActivityLoading => isActivityBeingAdded.value;

  UserModel user;

  updateNewActivitiesList(Activity activity) {
    if (newActivities.contains(activity)) return;

    removeExecutingActivityList(activity);
    removeEndedActivityList(activity);

    newActivities.add(activity..type = ActivityType.NEW);
    newActivities.refresh();
  }

  removeNewActivitiesList(Activity activity) {
    if (newActivities.contains(activity)) {
      newActivities.remove(activity);
      newActivities.refresh();
    }
  }

  updateExecutingActivitiesList(Activity activity) {
    if (executingActivities.contains(activity)) return;

    removeNewActivitiesList(activity);
    removeEndedActivityList(activity);

    executingActivities.add(activity..type = ActivityType.EXECUTING);
    executingActivities.refresh();
  }

  removeExecutingActivityList(Activity activity) {
    if (executingActivities.contains(activity)) {
      executingActivities.remove(activity);
      executingActivities.refresh();
    }
  }

  updateEndedActivitiesList(Activity activity) {
    if (endedActivities.contains(activity)) return;

    removeNewActivitiesList(activity);
    removeExecutingActivityList(activity);

    endedActivities.add(activity..type = ActivityType.ENDED);
    endedActivities.refresh();
  }

  removeEndedActivityList(Activity activity) {
    if (endedActivities.contains(activity)) {
      endedActivities.remove(activity);
      endedActivities.refresh();
    }
  }

  updateActivities() async {
    if (newActivities.isEmpty &&
        executingActivities.isEmpty &&
        endedActivities.isEmpty &&
        isFirstLoad) {
      user = UserModel.fromJson(jsonDecode(Get.parameters["user"]));
      await scheduleRepository.getScheduledActivities(user.email).then((value) {
        value.forEach((element) {
          if (element.type == ActivityType.EXECUTING) {
            executingActivities.add(element);
          } else if (element.type == ActivityType.ENDED) {
            endedActivities.add(element);
          } else {
            newActivities.add(element);
          }
        });

        if (isFirstLoad) {
          Get.back();
        }
        isFirstLoad = false;
      }).catchError((error) {
        if (isFirstLoad) {
          Get.back();
        }
        isFirstLoad = false;
      });
    }

    activities.clear();
    activities.addAll(newActivities);
    activities.addAll(executingActivities);
    activities.addAll(endedActivities);

    activities.sort((a, b) => (a.initDate).compareTo(b.initDate));
    activities.refresh();
  }

  int getActivitiesSize() {
    return activities.length;
  }

  scheduleNewActivity({
    Activity activity,
    bool shouldEditActivity,
  }) {
    if (activityForm.currentState.validate()) {
      if (shouldEditActivity) {
        newActivities.forEach((element) {
          if (element == activity) element = activity;
        });
      } else {
        newActivities.add(activity);
      }

      newActivities.refresh();
      updateActivities();
      Get.back();
    }
  }

  removeActivity(Activity activity) {
    removeNewActivitiesList(activity);
    removeExecutingActivityList(activity);
    removeEndedActivityList(activity);

    updateActivities();
  }

  saveActivitiesUpdated(BuildContext context) {
    updateActivities();
    isActivityBeingAdded.value = true;
    scheduleRepository.addActivity(activities, user.email).then((value) {
      isActivityBeingAdded.value = false;
      Utils.showSuccessSnackBar(context);
    }).catchError((error) {
      isActivityBeingAdded.value = false;
      Utils.showErrorSnackBar(context);
    });
  }
}
