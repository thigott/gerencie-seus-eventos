import 'package:flutter/material.dart';
import 'package:gerencie_seu_evento/core/styles/appcolors.style.dart';
import 'package:gerencie_seu_evento/core/utils/utils.dart';
import 'package:gerencie_seu_evento/modules/schedule/controllers/schedule.controller.dart';
import 'package:gerencie_seu_evento/modules/schedule/models/activity.model.dart';
import 'package:get/get.dart';

class ScheduleActivity extends GetView<ScheduleController> {
  ScheduleActivity({this.activity});

  final Activity activity;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(
        horizontal: 20,
        vertical: 10,
      ),
      child: Card(
        elevation: 4,
        color: AppColors.WHITE,
        child: Container(
          padding: EdgeInsets.all(20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                children: [
                  Container(
                    height: 16,
                    width: 20,
                    color: activity.determinePriorityColor(),
                  ),
                  SizedBox(width: 10),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(right: 30),
                      child: Center(
                        child: Text(
                          activity.title,
                          style: TextStyle(
                            fontSize: 18,
                          ),
                        ),
                      ),
                    ),
                  ),
                  IconButton(
                    onPressed: () => Utils.showAddActivityDialog(activity: activity),
                    icon: Icon(
                      Icons.edit_rounded,
                      color: AppColors.BLUE36,
                      size: 16,
                    ),
                  ),
                  IconButton(
                    onPressed: () => controller.removeActivity(activity),
                    icon: Icon(
                      Icons.delete_rounded,
                      color: AppColors.BLUE36,
                      size: 16,
                    ),
                  )
                ],
              ),
              SizedBox(height: 30),
              Text("Data de início: ${activity.initDate}"),
              SizedBox(height: 5),
              Text("Data de fim: ${activity.endDate}"),
              activity.responsible.isNotEmpty
                  ? Padding(
                      padding: const EdgeInsets.only(top: 10),
                      child: Text("Responsável: ${activity.responsible}"),
                    )
                  : Container(),
              activity.observation.isNotEmpty
                  ? Padding(
                      padding: const EdgeInsets.only(top: 10),
                      child: Text(
                        "Observações: ${activity.observation}",
                      ),
                    )
                  : Container(),
              activity.value.isNotEmpty
                  ? Padding(
                      padding: const EdgeInsets.only(top: 10),
                      child: Text(
                        "Valor: ${activity.value}",
                      ),
                    )
                  : Container(),
            ],
          ),
        ),
      ),
    );
  }
}
