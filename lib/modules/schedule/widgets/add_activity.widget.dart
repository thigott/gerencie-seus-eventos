import 'package:flutter/material.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:gerencie_seu_evento/core/styles/appicons.style.dart';
import 'package:gerencie_seu_evento/core/utils/masks.util.dart';
import 'package:gerencie_seu_evento/core/validators/date.validator.dart';
import 'package:gerencie_seu_evento/core/validators/default_required.validator.dart';
import 'package:gerencie_seu_evento/core/widgets/buttons/custom.button.dart';
import 'package:gerencie_seu_evento/core/widgets/forms/custom.form.dart';
import 'package:gerencie_seu_evento/modules/schedule/controllers/schedule.controller.dart';
import 'package:gerencie_seu_evento/modules/schedule/models/activity.model.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';

class AddActivity extends GetView<ScheduleController> {
  AddActivity({this.initialActivity});

  final Activity initialActivity;

  @override
  Widget build(BuildContext context) {
    Activity activity = initialActivity == null ? Activity() : initialActivity;

    return GestureDetector(
      onTap: () {
        FocusManager.instance.primaryFocus.unfocus();
      },
      child: SingleChildScrollView(
        padding: EdgeInsets.all(20),
        child: Column(
          children: [
            Center(
              child: Lottie.asset(
                AppIcons.ADD_TASK,
                width: 300,
                height: 250,
              ),
            ),
            Container(
              width: 900,
              child: Card(
                elevation: 10,
                color: Colors.white,
                child: Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(top: 20.0),
                        child: Form(
                          key: controller.activityForm,
                          child: Column(
                            children: [
                              CustomTextFormField(
                                value: activity.title,
                                label: "Título da atividade",
                                onChanged: (value) => activity.title = value,
                                validator: MultiValidator(
                                  [DefaultRequiredValidator()],
                                ),
                              ),
                              SizedBox(height: 10),
                              CustomTextFormField(
                                value: activity.initDate,
                                label: "Data de início (dd/MM/YYYY)",
                                onChanged: (value) => activity.initDate = value,
                                inputFormatters: [
                                  MaskTextInputFormatter(mask: Masks.DATE)
                                ],
                                validator: MultiValidator(
                                  [
                                    DefaultRequiredValidator(),
                                    CustomDateValidator(),
                                  ],
                                ),
                              ),
                              SizedBox(height: 10),
                              CustomTextFormField(
                                value: activity.endDate,
                                label: "Data de fim (dd/MM/YYYY)",
                                labelExtend: "(previsão)",
                                inputFormatters: [
                                  MaskTextInputFormatter(mask: Masks.DATE)
                                ],
                                onChanged: (value) => activity.endDate = value,
                                validator: MultiValidator(
                                  [
                                    DefaultRequiredValidator(),
                                    CustomDateValidator(),
                                  ],
                                ),
                              ),
                              SizedBox(height: 10),
                              CustomTextFormField(
                                value: activity.responsible,
                                label: "Responsável (opcional)",
                                onChanged: (value) =>
                                    activity.responsible = value,
                              ),
                              SizedBox(height: 10),
                              CustomTextFormField(
                                value: activity.observation,
                                label: "Observações (opcional)",
                                onChanged: (value) =>
                                    activity.observation = value,
                              ),
                              SizedBox(height: 10),
                              CustomTextFormField(
                                value: activity.value,
                                label: "Valor (opcional)",
                                keyboardType: TextInputType.number,
                                controller: MoneyMaskedTextController(
                                    decimalSeparator: ",",
                                    thousandSeparator: "."),
                                onChanged: (value) =>
                                    activity.value = "R\$ $value",
                              ),
                              SizedBox(height: 10),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(height: 5),
                      Padding(
                        padding: const EdgeInsets.only(left: 4.0),
                        child: Center(
                          child: Row(
                            children: [
                              Text(
                                "Prioridade: ",
                              ),
                              lowPrioryOption(activity),
                              mediumPrioryOption(activity),
                              highPrioryOption(activity),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(height: 10),
                      Obx(
                        () => Padding(
                          padding:
                              const EdgeInsets.all(20.0).copyWith(bottom: 0),
                          child: Container(
                            width: 200,
                            child: CustomButton(
                              label: initialActivity == null
                                  ? "CADASTRAR"
                                  : "SALVAR",
                              onTap: () => controller.scheduleNewActivity(
                                activity: activity,
                                shouldEditActivity: initialActivity != null,
                              ),
                              enabled: controller.isActivityLoading != true,
                              loadingIndicator:
                                  controller.isActivityLoading != true
                                      ? null
                                      : Center(
                                          child: SizedBox(
                                            width: 20,
                                            height: 20,
                                            child: CircularProgressIndicator(
                                              valueColor:
                                                  AlwaysStoppedAnimation<Color>(
                                                Colors.white,
                                              ),
                                            ),
                                          ),
                                        ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget lowPrioryOption(Activity activity) {
    return Obx(
      () => Row(
        children: [
          Radio(
            value: ActivityPriority.LOW,
            groupValue: activity.priority.value,
            onChanged: (state) =>
                activity.priority.value = ActivityPriority.LOW,
          ),
          Text("Baixa"),
        ],
      ),
    );
  }

  Widget mediumPrioryOption(Activity activity) {
    return Obx(
      () => Row(
        children: [
          Radio(
            value: ActivityPriority.MEDIUM,
            groupValue: activity.priority.value,
            onChanged: (state) =>
                activity.priority.value = ActivityPriority.MEDIUM,
          ),
          Text("Média"),
        ],
      ),
    );
  }

  Widget highPrioryOption(Activity activity) {
    return Obx(
      () => Row(
        children: [
          Radio(
            value: ActivityPriority.HIGH,
            groupValue: activity.priority.value,
            onChanged: (state) =>
                activity.priority.value = ActivityPriority.HIGH,
          ),
          Text("Alta"),
        ],
      ),
    );
  }
}
