import 'dart:convert';

import 'package:gerencie_seu_evento/core/interfaces/api.interface.dart';
import 'package:gerencie_seu_evento/core/routes/api_routes.dart';
import 'package:gerencie_seu_evento/modules/schedule/models/activity.model.dart';
import 'package:get/get.dart';

class ScheduleRepository {
  ScheduleRepository(this._iApi);
  IApi _iApi;

  Future<bool> addActivity(RxList<dynamic> activities, String email) async {
    try {
      var result =
          await _iApi.call(EApiType.post, ApiRoutes.ADD_ACTIVITY, data: {
        "nome": "Evento $email",
        "atividades": jsonEncode(activities.map((e) => e.toJson()).toList())
      });

      return result.data["response"];
    } catch (e) {
      return null;
    }
  }

  Future<List<Activity>> getScheduledActivities(String email) async {
    try {
      var result = await _iApi.call(
        EApiType.post,
        ApiRoutes.GET_EVENT_DETAILS,
        data: {"nome": "Evento $email"},
      );

      List<Activity> response = [];
      (result.data["response"]["atividades"] as List).forEach((element) {
        response.add(Activity.fromJson(element));
      });

      return response;
    } catch (e) {
      return null;
    }
  }
}
