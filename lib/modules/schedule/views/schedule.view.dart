import 'package:flutter/material.dart';
import 'package:gerencie_seu_evento/core/styles/appcolors.style.dart';
import 'package:gerencie_seu_evento/core/utils/utils.dart';
import 'package:gerencie_seu_evento/core/widgets/buttons/custom.button.dart';
import 'package:gerencie_seu_evento/modules/schedule/controllers/schedule.controller.dart';
import 'package:gerencie_seu_evento/modules/schedule/models/activity.model.dart';
import 'package:gerencie_seu_evento/modules/schedule/widgets/schedule_activity.widget.dart';
import 'package:get/get.dart';

class ScheduleView extends GetView<ScheduleController> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(20.0),
      child: Column(
        children: [
          Expanded(
            child: Stack(
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Expanded(
                      flex: 1,
                      child: newActivitiesContainer(),
                    ),
                    SizedBox(width: 20),
                    Expanded(
                      flex: 1,
                      child: executingActivitiesContainer(),
                    ),
                    SizedBox(width: 20),
                    Expanded(
                      flex: 1,
                      child: endedActivitiesContainer(),
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Align(
                    alignment: Alignment.bottomRight,
                    child: FloatingActionButton(
                      backgroundColor: AppColors.BLUE36,
                      onPressed: () => Utils.showAddActivityDialog(),
                      child: Icon(Icons.add),
                    ),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 20),
          Obx(
            () => Container(
              width: 200,
              child: CustomButton(
                label: "Salvar",
                onTap: () => controller.saveActivitiesUpdated(context),
                enabled: controller.isActivityLoading != true,
                loadingIndicator: controller.isActivityLoading != true
                    ? null
                    : Center(
                        child: SizedBox(
                          width: 20,
                          height: 20,
                          child: CircularProgressIndicator(
                            valueColor: AlwaysStoppedAnimation<Color>(
                              Colors.white,
                            ),
                          ),
                        ),
                      ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget newActivitiesContainer() {
    return DragTarget(
      builder: (context, accepted, rejected) {
        return Container(
          decoration: BoxDecoration(
            border: Border.all(
              color: AppColors.GRAY8A,
              width: 1,
            ),
            borderRadius: BorderRadius.circular(20),
          ),
          child: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(height: 10),
                Text(
                  "Novas Atividades",
                  style: TextStyle(
                    color: AppColors.BLACK03,
                    fontWeight: FontWeight.bold,
                    fontSize: 19,
                  ),
                ),
                SizedBox(height: 10),
                Obx(
                  () => ListView.separated(
                    itemBuilder: (_, index) {
                      final activity = controller.newActivities[index];
                      return Draggable<Activity>(
                        data: activity,
                        feedback: ScheduleActivity(
                          activity: activity,
                        ),
                        child: ScheduleActivity(
                          activity: activity,
                        ),
                      );
                    },
                    separatorBuilder: (_, __) => Container(height: 10),
                    itemCount: controller.newActivities.length,
                    shrinkWrap: true,
                  ),
                ),
              ],
            ),
          ),
        );
      },
      onAccept: (Activity data) {
        controller.updateNewActivitiesList(data);
      },
    );
  }

  Widget executingActivitiesContainer() {
    return DragTarget(
      builder: (context, accepted, rejected) {
        return Container(
          decoration: BoxDecoration(
            border: Border.all(
              color: AppColors.GRAY8A,
              width: 1,
            ),
            borderRadius: BorderRadius.circular(20),
          ),
          child: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(height: 10),
                Text(
                  "Atividades em execução",
                  style: TextStyle(
                    color: AppColors.BLACK03,
                    fontWeight: FontWeight.bold,
                    fontSize: 19,
                  ),
                ),
                SizedBox(height: 10),
                Obx(
                  () => ListView.separated(
                    itemBuilder: (_, index) {
                      final activity = controller.executingActivities[index];
                      return Draggable<Activity>(
                        data: activity,
                        feedback: ScheduleActivity(
                          activity: activity,
                        ),
                        child: ScheduleActivity(
                          activity: activity,
                        ),
                      );
                    },
                    separatorBuilder: (_, __) => Container(height: 10),
                    itemCount: controller.executingActivities.length,
                    shrinkWrap: true,
                  ),
                ),
              ],
            ),
          ),
        );
      },
      onAccept: (Activity data) {
        controller.updateExecutingActivitiesList(data);
      },
    );
  }

  Widget endedActivitiesContainer() {
    return DragTarget(
      builder: (context, accepted, rejected) {
        return Container(
          decoration: BoxDecoration(
            border: Border.all(
              color: AppColors.GRAY8A,
              width: 1,
            ),
            borderRadius: BorderRadius.circular(20),
          ),
          child: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(height: 10),
                Text(
                  "Atividades encerradas",
                  style: TextStyle(
                    color: AppColors.BLACK03,
                    fontWeight: FontWeight.bold,
                    fontSize: 19,
                  ),
                ),
                SizedBox(height: 10),
                Obx(
                  () => ListView.separated(
                    itemBuilder: (_, index) {
                      final activity = controller.endedActivities[index];
                      return Draggable<Activity>(
                        data: activity,
                        feedback: ScheduleActivity(
                          activity: activity,
                        ),
                        child: ScheduleActivity(
                          activity: activity,
                        ),
                      );
                    },
                    separatorBuilder: (_, __) => Container(height: 10),
                    itemCount: controller.endedActivities.length,
                    shrinkWrap: true,
                  ),
                ),
              ],
            ),
          ),
        );
      },
      onAccept: (Activity data) {
        controller.updateEndedActivitiesList(data);
      },
    );
  }
}
