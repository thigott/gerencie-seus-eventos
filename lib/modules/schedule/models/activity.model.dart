import 'package:flutter/cupertino.dart';
import 'package:gerencie_seu_evento/core/styles/appcolors.style.dart';
import 'package:get/get.dart';

class Activity {
  Activity({
    this.title,
    this.initDate,
    this.endDate,
    this.observation = "",
    this.value = "",
    this.responsible = "",
  });

  String title;
  String initDate;
  String endDate;
  String observation;
  String value;
  String responsible;
  final priority = ActivityPriority.LOW.obs;
  ActivityType type = ActivityType.NEW;

  Color determinePriorityColor() {
    switch (priority.value) {
      case ActivityPriority.HIGH:
        return AppColors.REDF1;
        break;

      case ActivityPriority.MEDIUM:
        return AppColors.CONTAINER_ORANGE;
        break;

      default:
        return AppColors.YELLOW;
    }
  }

  String determineStatus() {
    switch (type) {
      case ActivityType.EXECUTING:
        {
          return "Executando";
        }
      case ActivityType.ENDED:
        {
          return "Finalizada";
        }
      default:
        {
          return "Nova";
        }
    }
  }

  Map<String, dynamic> toJson() => {
        "title": title == null ? null : title,
        "initDate": initDate == null ? null : initDate,
        "endDate": endDate == null ? null : endDate,
        "observation": observation == null ? null : observation,
        "value": value == null ? null : value,
        "responsible": responsible == null ? null : responsible,
        "type": type == null ? null : type.name,
      };

  static Activity fromJson(Map<String, dynamic> data) {
    return Activity(
        title: data["title"],
        initDate: data["initDate"],
        endDate: data["endDate"],
        observation: data["observation"],
        value: data["value"],
        responsible: data["responsible"])
      ..type = ActivityType.values
          .firstWhere((element) => element.name == data["type"]);
  }
}

enum ActivityPriority {
  LOW,
  MEDIUM,
  HIGH,
}

enum ActivityType { NEW, EXECUTING, ENDED }
