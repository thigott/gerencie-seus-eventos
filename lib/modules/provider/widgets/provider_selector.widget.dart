import 'package:flutter/material.dart';
import 'package:gerencie_seu_evento/core/styles/appcolors.style.dart';
import 'package:gerencie_seu_evento/modules/provider/controllers/provider.controller.dart';
import 'package:get/get.dart';

class ProviderSelector extends GetView<ProviderController> {
  @override
  Widget build(BuildContext context) {
    return Obx(
      () => ListView.separated(
        itemBuilder: (_, index) {
          final provider = controller.providers[index];
          return GestureDetector(
            onTap: () => controller.updateProviderSelected(provider),
            behavior: HitTestBehavior.translucent,
            child: Text(
              provider,
              style: TextStyle(
                fontSize: 21,
                color: provider == controller.getProvider()
                    ? AppColors.BLACK03
                    : AppColors.GRAY8A,
                fontWeight: provider == controller.getProvider()
                    ? FontWeight.bold
                    : FontWeight.normal,
              ),
            ),
          );
        },
        separatorBuilder: (_, __) => Container(width: 20),
        itemCount: controller.providers.length,
        shrinkWrap: true,
        scrollDirection: Axis.horizontal,
      ),
    );
  }
}
