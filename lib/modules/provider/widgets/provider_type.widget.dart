import 'package:flutter/material.dart';
import 'package:gerencie_seu_evento/modules/provider/widgets/provider_item.widget.dart';

class ProviderType extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(height: 10),
        Expanded(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 80),
            child: GridView.count(
              shrinkWrap: true,
              childAspectRatio: MediaQuery.of(context).size.height / 450,
              crossAxisCount: 3,
              children: List.generate(
                9,
                (index) => Center(child: ProviderItem()),
              ),
            ),
          ),
        )
      ],
    );
  }
}
