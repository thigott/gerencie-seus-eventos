import 'package:flutter/material.dart';
import 'package:gerencie_seu_evento/core/styles/appcolors.style.dart';
import 'package:gerencie_seu_evento/core/styles/appicons.style.dart';

class ProviderItem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 10,
      color: Colors.white,
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20),
            topRight: Radius.circular(20),
          ),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              height: 80,
              child: Image.asset(
                AppIcons.STORE_IMAGE,
                fit: BoxFit.fill,
              ),
            ),
            SizedBox(height: 8),
            Text(
              "Fornecedor x",
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 18),
            ),
            Text(
              "Avenida exemplo, n 45",
              textAlign: TextAlign.center,
            ),
            Text(
              "Pedido mínimo: 12 itens",
              textAlign: TextAlign.center,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(
                  Icons.star,
                  color: AppColors.YELLOW,
                ),
                Text("4 estrelas"),
              ],
            ),
            SizedBox(height: 8),
          ],
        ),
      ),
    );
  }
}
