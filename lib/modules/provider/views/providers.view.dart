import 'package:flutter/material.dart';
import 'package:gerencie_seu_evento/core/styles/appcolors.style.dart';
import 'package:gerencie_seu_evento/modules/main/widgets/menu.widget.dart';
import 'package:gerencie_seu_evento/modules/provider/widgets/provider_selector.widget.dart';
import 'package:gerencie_seu_evento/modules/provider/widgets/provider_type.widget.dart';

class ItemProviders extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.BLUEED,
      body: Row(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          MenuApp(
            itemSelected: "Encontre um fornecedor",
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Padding(
                  padding:
                      const EdgeInsets.all(20.0).copyWith(bottom: 0, top: 24),
                  child: Container(
                    height: 24,
                    child: ProviderSelector(),
                  ),
                ),
                Expanded(
                  flex: 9,
                  child: Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: Container(
                      decoration: BoxDecoration(
                        color: AppColors.WHITE,
                        borderRadius: BorderRadius.circular(10),
                      ),
                      padding: EdgeInsets.all(10),
                      child: ProviderType(),
                    ),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
