import 'package:get/get.dart';

class ProviderController {
  var providers = [
    "Bebidas",
    "Infraestrutura",
    "Local",
    "Atração",
  ].obs;

  var providerSelected = "Bebidas".obs;

  getProvider() => providerSelected.value;

  updateProviderSelected(String provider) {
    providerSelected.value = provider;
    providers.refresh();
  }
}
