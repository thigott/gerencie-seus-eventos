import 'package:flutter/material.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:gerencie_seu_evento/core/styles/appicons.style.dart';
import 'package:gerencie_seu_evento/core/styles/appstyles.style.dart';
import 'package:gerencie_seu_evento/core/utils/masks.util.dart';
import 'package:gerencie_seu_evento/core/validators/birth_date.validator.dart';
import 'package:gerencie_seu_evento/core/validators/default_required.validator.dart';
import 'package:gerencie_seu_evento/core/validators/document.validator.dart';
import 'package:gerencie_seu_evento/core/widgets/buttons/custom.button.dart';
import 'package:gerencie_seu_evento/core/widgets/forms/custom.form.dart';
import 'package:gerencie_seu_evento/core/widgets/forms/custom_dropdown_menu.form.dart';
import 'package:gerencie_seu_evento/modules/signup/controllers/signup.controller.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:lottie/lottie.dart';
import 'package:mask_shifter/mask_shifter.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';

class SignUpView extends GetView<SignUpController> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusManager.instance.primaryFocus.unfocus();
      },
      child: SingleChildScrollView(
        padding: EdgeInsets.all(20),
        child: Column(
          children: [
            Center(
              child: Lottie.asset(AppIcons.ASK_INFORMATION,
                  width: 300, height: 250),
            ),
            Container(
              width: 900,
              child: Card(
                elevation: 10,
                color: Colors.white,
                child: Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Column(
                    children: [
                      Text(
                        "Precisamos de algumas informações \nmas fique tranquilo é rápido e seguro!",
                        style: AppStyles.defaultTitle,
                        textAlign: TextAlign.center,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 20.0),
                        child: Form(
                          key: controller.signUpForm,
                          child: Column(
                            children: [
                              CustomTextFormField(
                                label: "Nome completo",
                                onChanged: (value) =>
                                    controller.user.name = value,
                                validator: MultiValidator([
                                  DefaultRequiredValidator(),
                                ]),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Expanded(
                                    child: CustomTextFormField(
                                      label: "Nascimento",
                                      onChanged: (value) =>
                                          controller.user.birthDate = value,
                                      inputFormatters: [
                                        MaskTextInputFormatter(mask: Masks.DATE)
                                      ],
                                      keyboardType: TextInputType.number,
                                      validator: MultiValidator([
                                        DefaultRequiredValidator(),
                                        BirthDateValidator(),
                                      ]),
                                    ),
                                  ),
                                  Container(width: 15),
                                  Obx(
                                    () => Expanded(
                                      child: CustomDropdownMenu(
                                        label: "Sexo",
                                        hint: "Escolha",
                                        items: ["Masculino", "Feminino"],
                                        selected: controller
                                                .user.gender.value.isNotEmpty
                                            ? controller.user.gender.value
                                            : null,
                                        onChanged: (value) => controller
                                            .user.gender.value = value,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              CustomTextFormField(
                                label: "Documento (CPF ou CNPJ)",
                                onChanged: (value) =>
                                    controller.user.cnpj = value,
                                inputFormatters: [
                                  MaskedTextInputFormatterShifter(
                                    maskONE: Masks.CPF,
                                    maskTWO: Masks.CNPJ,
                                  )
                                ],
                                validator: MultiValidator([
                                  DefaultRequiredValidator(),
                                  DocumentValidator(),
                                ]),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              CustomTextFormField(
                                label: "Razão social",
                                onChanged: (value) =>
                                    controller.user.socialReason = value,
                                validator: MultiValidator([
                                  DefaultRequiredValidator(),
                                ]),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              CustomTextFormField(
                                label: "Nome Fantasia",
                                onChanged: (value) =>
                                    controller.user.fantasyName = value,
                                validator: MultiValidator([
                                  DefaultRequiredValidator(),
                                ]),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              CustomTextFormField(
                                label: "CEP",
                                onChanged: (value) =>
                                    controller.user.cep = value,
                                inputFormatters: [
                                  MaskTextInputFormatter(mask: Masks.CEP)
                                ],
                                validator: MultiValidator([
                                  DefaultRequiredValidator(),
                                ]),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              CustomTextFormField(
                                label: "E-mail",
                                onChanged: (value) =>
                                    controller.user.email = value,
                                validator: MultiValidator([
                                  DefaultRequiredValidator(),
                                  EmailValidator(errorText: "Email inválido"),
                                ]),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              CustomTextFormField(
                                label: "Senha",
                                onChanged: (value) =>
                                    controller.user.password = value,
                                obscureText: true,
                                validator: MultiValidator([
                                  DefaultRequiredValidator(),
                                ]),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              CustomTextFormField(
                                  label: "Complemento",
                                  onChanged: (value) =>
                                      controller.user.complement = value,
                                  labelExtend: "(opcional)"),
                            ],
                          ),
                        ),
                      ),
                      Obx(
                        () => Padding(
                          padding:
                              const EdgeInsets.all(20.0).copyWith(bottom: 0),
                          child: Container(
                            width: 200,
                            child: CustomButton(
                              label: "CADASTRAR",
                              onTap: () => controller.registerUser(),
                              enabled: controller.isLoading != true,
                              loadingIndicator: controller.isLoading != true
                                  ? null
                                  : Center(
                                      child: SizedBox(
                                        width: 20,
                                        height: 20,
                                        child: CircularProgressIndicator(
                                          valueColor:
                                              AlwaysStoppedAnimation<Color>(
                                                  Colors.white),
                                        ),
                                      ),
                                    ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
