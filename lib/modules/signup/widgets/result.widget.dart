import 'package:flutter/material.dart';
import 'package:gerencie_seu_evento/core/styles/appcolors.style.dart';
import 'package:gerencie_seu_evento/core/styles/appicons.style.dart';
import 'package:gerencie_seu_evento/core/widgets/buttons/custom.button.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';

class SignUpResult extends StatelessWidget {
  SignUpResult({this.isError = false});

  bool isError;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(20.0),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          isError
              ? Lottie.asset(
                  AppIcons.FAILED,
                  height: 400,
                  width: 400,
                )
              : Lottie.asset(
                  AppIcons.SUCCESS,
                  height: 400,
                  width: 400,
                ),
          SizedBox(
            height: 20,
          ),
          Text(
            isError
                ? "Ops! \nAlgo não saiu como planejado, tente novamente mais tarde!"
                : "Tudo certo! Conta criada! \nVocê está pronto para começar a organizar seus eventos!",
            style: TextStyle(fontSize: 21, color: AppColors.GRAY4F),
            textAlign: TextAlign.center,
          ),
          Spacer(),
          CustomButton(
            label: "Vamos lá!",
            onTap: () => Get.back(),
          )
        ],
      ),
    );
  }
}
