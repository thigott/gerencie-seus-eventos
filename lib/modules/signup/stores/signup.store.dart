import 'package:gerencie_seu_evento/core/utils/utils.dart';
import 'package:gerencie_seu_evento/modules/signup/models/user.model.dart';
import 'package:gerencie_seu_evento/modules/signup/repositories/signup.repository.dart';

import 'package:get/get.dart';

class SignUpStore {
  SignUpStore(this._repository);
  SignUpRepository _repository;

  final user = UserModel().obs;
  final isLoading = false.obs;

  registerUser() async {
    isLoading.value = true;

    _repository.registerUserOnServer(userModel: user.value).then((value) {
      isLoading.value = false;
      Get.back();
      Future.delayed(Duration(microseconds: 500),
          () => Utils.showSignUpResult(isError: !value));
    }).catchError((error) {
      isLoading.value = false;
    });
  }
}
