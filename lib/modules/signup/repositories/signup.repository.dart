import 'package:gerencie_seu_evento/core/interfaces/api.interface.dart';
import 'package:gerencie_seu_evento/core/routes/api_routes.dart';
import 'package:gerencie_seu_evento/modules/signup/models/user.model.dart';

class SignUpRepository {
  SignUpRepository(this._api);
  IApi _api;

  Future<bool> registerUserOnServer({UserModel userModel}) async {
    try {
      var result = await _api.call(
        EApiType.post,
        ApiRoutes.USER_REGISTER,
        data: userModel.toJson(),
      );

      return true;
    } catch (e) {
      return null;
    }
  }
}
