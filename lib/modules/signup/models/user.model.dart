import 'package:get/get.dart';

class UserModel {
  UserModel({
    this.name,
    this.birthDate,
    this.cnpj,
    this.socialReason,
    this.fantasyName,
    this.cep,
    this.address,
    this.complement,
    this.email,
    this.password,
  });

  String name;
  String birthDate;
  final gender = "".obs;
  String cnpj;
  String socialReason;
  String fantasyName;
  String cep;
  String address;
  String complement;
  String email;
  String password;

  Map<String, dynamic> toJson() => {
        "nome": name == null ? null : name,
        "nascimento": birthDate == null ? null : birthDate,
        "genero": gender.value == null ? null : gender.value,
        "cnpj": cnpj == null ? null : cnpj,
        "razaoSocial": socialReason == null ? null : socialReason,
        "nomeFantasia": fantasyName == null ? null : fantasyName,
        "cep": cep == null ? null : cep,
        "email": email == null ? null : email,
        "senha": password == null ? null : password,
        "complemento": complement == null ? null : complement
      };

  static UserModel fromJson(Map<String, dynamic> data) {
    return UserModel(
      name: data["nome"],
      birthDate: data["nascimento"],
      cnpj: data["cnpj"],
      socialReason: data["razaoSocial"],
      fantasyName: data["nomeFantasia"],
      cep: data["cep"],
      complement: data["complement"],
      email: data["email"],
      password: data["senha"],
    );
  }
}
