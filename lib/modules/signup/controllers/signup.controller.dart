import 'package:flutter/material.dart';
import 'package:gerencie_seu_evento/modules/signup/models/user.model.dart';
import 'package:gerencie_seu_evento/modules/signup/stores/signup.store.dart';
import 'package:get/get_state_manager/get_state_manager.dart';

import 'package:get/get.dart';

class SignUpController extends GetxController {
  SignUpController(this._store);
  SignUpStore _store;

  final GlobalKey<FormState> signUpForm = GlobalKey();
  bool get isLoading => _store.isLoading.value;

  UserModel get user => _store.user.value;

  registerUser() {
    if (signUpForm.currentState.validate()) {
      _store.registerUser();
    }
  }
}
