import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:gerencie_seu_evento/core/styles/appcolors.style.dart';
import 'package:gerencie_seu_evento/core/styles/appicons.style.dart';
import 'package:gerencie_seu_evento/core/styles/appstyles.style.dart';
import 'package:gerencie_seu_evento/core/utils/utils.dart';
import 'package:gerencie_seu_evento/core/widgets/buttons/custom.button.dart';

class HomeView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(
            image: DecorationImage(
                fit: BoxFit.fill, image: AssetImage(AppIcons.BACKGROUND_SHOW))),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.all(12.0),
              child: Row(
                children: [
                  SvgPicture.asset(
                    AppIcons.BILL,
                    color: AppColors.WHITE,
                    width: 30,
                    height: 30,
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Text(
                    "Gerencie seus Eventos | Soluções",
                    style: AppStyles.mainSubTitle,
                  ),
                  Spacer(),
                  Container(
                    width: 210,
                    child: CustomButton(
                      label: "LOGAR",
                      radius: 20,
                      onTap: () => Utils.showLoginDialog(),
                      backgroundColor: Colors.white,
                      textColor: AppColors.BLUE36,
                    ),
                  ),
                  SizedBox(width: 20),
                  Container(
                    width: 210,
                    child: CustomButton(
                      label: "CRIE SUA CONTA",
                      radius: 20,
                      onTap: () => Utils.showSignUpDialog(),
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Text(
                      "Produza e Gerencie seus eventos \nutilizando a melhor plataforma do Brasil!",
                      style: AppStyles.mainTitle,
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Text(
                      "Soluções completas para produtores de eventos e empreendores digitais.",
                      style: AppStyles.mainSubTitle,
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      "Estamos aqui para te auxiliar na gestão e focar naquilo que realmente importa!",
                      style: AppStyles.mainSubTitle,
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(
                      height: 110,
                    ),
                    Container(
                      padding:
                          EdgeInsets.symmetric(horizontal: 15, vertical: 18),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(80),
                      ),
                      child: Padding(
                        padding: const EdgeInsets.only(top: 2.0),
                        child: SvgPicture.asset(
                          AppIcons.ARROW_DOWN,
                          width: 15,
                          height: 15,
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 40,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
