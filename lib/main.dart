import 'package:flutter/material.dart';

import 'core/main/views/main.view.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  runApp(MyApp());
}